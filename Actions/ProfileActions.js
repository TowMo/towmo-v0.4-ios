'use strict'

var Reflux = require("reflux");
var ProfileActions = Reflux.createActions({
	"getAuth": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"removeRequestId": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"setFilter":  {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"setPreferredSiteRight":  {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"setRequestId": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"logoutUser": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"setCurrentUser": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
});

module.exports = ProfileActions;