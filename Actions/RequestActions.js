'use strict'

var Reflux = require("reflux");
var RequestActions = Reflux.createActions({
	"addRequest": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"addImgToDb": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"addImgToHost": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"addStatus": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"buildImgObj": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"endListeners": {
		asyncResult: false,
		shouldEmit: function() {
			return true;
		}
	},
	"extractNextStatuses": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"getImgTemplates": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"getRequest": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"pullRequest": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"pullRequests": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"pullVehicleData": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"refreshRequests": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"removeFromImages": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"setParam": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"setSiteId": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"setTodoStatus": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"setVehicle": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
	"updateVehicle": {
		asyncResult: true,
		shouldEmit: function() {
			return true;
		}
	},
});

module.exports = RequestActions;