'use strict';

// REACT PARTS
var Display = require('react-native-device-display');
var Icon = require("react-native-vector-icons/Ionicons");
var React = require("react-native");
var PickerIOS = require('react-native-multipicker');

// COMPONENTS
var LineSeparator = require('../Comps/LineSeparator');
var SavePanel = require('../Comps/SavePanel');

// MIXINS
var SiteMixin = require("../Mixins/Site");
var ViewMixin = require("../Mixins/View");

// Utilities
var Moment = require("moment");
var _ = require("lodash");

var MAX_HOURS = 12
	, HOURS = [1,2,3,4,5,6,7,8,9,10,11,12]
	, MINUTES = [0,5,10,15,20,25,30,35,40,45,50,55]
	, PRIOR_DAY_COUNT = 10
	, MERIDIEMS = new Array("AM", "PM");
	
var {
	AlertIOS,
	// Modal,
	PropTypes,
	StatusBarIOS,
	StyleSheet,
	Text,
	TouchableHighlight,
	View,
} = React;

var WhenScene = React.createClass({
	mixins: [SiteMixin, ViewMixin],
	propTypes: {
		closeDisplay: React.PropTypes.func,
		initialVal: PropTypes.string,
		lookups: PropTypes.object,
		setDate: PropTypes.func,
		style: React.PropTypes.number,
  },

	_styles: StyleSheet.create({
		main: {
			backgroundColor: "#000000",
			flexDirection: "column",
			height: Display.height - ViewMixin.Dimensions.STATUS_BAR_HEIGHT,
			top: ViewMixin.Dimensions.STATUS_BAR_HEIGHT,
			width: Display.width,
		},
		dateDisplay: {
			backgroundColor: "#2E2E2E",
			borderColor: "#A4A4A4",
			borderBottomWidth: 0.5,
			flexDirection: "row",
			paddingVertical: 10,
			paddingHorizontal: 3,
		}, ddText: {
			color: "#01DF01",
			fontFamily: "System",
			fontSize: 20,
			fontWeight: "200",
			textAlign: "center",
		}, exit: {
			alignItems: "center",
			backgroundColor: "#1C1C1C",
			borderTopWidth: 1,
			borderColor: "#A4A4A4",
			bottom: 0,
			height: ViewMixin.Dimensions.TAB_BAR_HEIGHT,
			paddingVertical: 3,
			paddingHorizontal: 8,
			position: "absolute",
			width: Display.width,
		}, exitIcon: {
			color: "#31B404",
			fontSize: 44,
		}
	}),
	_meridiemIndex: -1,
	_priorDays: [],

	getInitialState: function() {		
		return {
			chosenDate: this._resetTimestamp()
		};
	},

	componentWillMount: function() {
		this._priorDays = _.times(PRIOR_DAY_COUNT, (priorDay) => {
			return Moment().subtract(priorDay, 'day')._d;
		}).reverse();

		this._proposeDate(this.state.chosenDate);
		// this._meridiemIndex = this._findMeridiemIndex(this.state.chosenDate);
	},

	componentWillUpdate: function(newProps, newState) {
		// if (newProps.resetData)
		// 	this.props.setTopicData("when", this.state.chosenDate, true);
	},

	_adjustMinute: function(date) {
		var minute = date.getMinutes();
		var minutes = {
			floor: _.floor(minute, -1),
			rounded: _.round(minute, -1)
		};

		var newMinutes;
		
		if (minutes.floor === minutes.rounded)
			newMinutes = minutes.floor;
		else {
			var ceilingIndex = _.indexOf(MINUTES, minutes.floor) + 1;
			newMinutes = MINUTES[ceilingIndex];
		}

		return Moment(date.setMinutes(newMinutes)).toDate();
	},

	_findMeridiemIndex: function(date) {
		return (date.getHours() < 12) ? 0 : 1;
	},

	_getChosenDateCopy: function() {
		return _.cloneDeep(this.state.chosenDate);
	},

	_getNow: function() {
		var now = Moment().toDate();
		
		if ( !_.contains(MINUTES, now.getMinutes()) )
			now = this._adjustMinute(now);

		return now;
	},

	_proposeDate: function(newTimestamp) {
		// set everything EXCEPT for time;
		var tempDate = this._getChosenDateCopy();

		tempDate.setYear(newTimestamp.getFullYear());
		tempDate.setMonth(newTimestamp.getMonth());
		tempDate.setDate(newTimestamp.getDate());

		this._vetProposedTimestamp("date", tempDate);
	},

	_proposeMeridiem: function(newTimestamp) {
		// set everything EXCEPT for date;
		var prevChosenDate = this._getChosenDateCopy();
		prevChosenDate.setHours(newTimestamp.getHours());

		this._vetProposedTimestamp("meridiem", prevChosenDate);
	},

	_proposeTime: function(newTimestamp) {
		// set everything EXCEPT for date;
		var prevChosenDate = this._getChosenDateCopy();

		prevChosenDate.setMilliseconds(newTimestamp.getMilliseconds());
		prevChosenDate.setSeconds(newTimestamp.getSeconds());
		prevChosenDate.setMinutes(newTimestamp.getMinutes());
		prevChosenDate.setHours(newTimestamp.getHours());

		this._vetProposedTimestamp("time", prevChosenDate);
	},

	_resetTimestamp: function(setState) {
		var initialDate = _.isEmpty(this.props.initialVal) ? "" : Moment(this.props.initialVal).toDate();
		var newDate =  initialDate || this._getNow();
		this._meridiemIndex = this._findMeridiemIndex(newDate);

		if (setState)
			this.setState({chosenDate: newDate});
		else
			return newDate;
	},

	_setTimestamp: function() {
		this.props.closeDisplay();
		this.props.setDate(Moment(this.state.chosenDate).format());
	},

	_updateTimestamp: function(type, updatedTimestamp) {
		this._meridiemIndex = this._findMeridiemIndex(updatedTimestamp);
		this.setState({chosenDate: updatedTimestamp});
	},

	_vetProposedTimestamp: function(type, proposedDate) {
		// var now = this._adjustMinute(Moment().toDate());
		var now = this._getNow();

		if ( Moment(proposedDate).isBefore(now) || Moment(proposedDate).isSame(now) )
			this._updateTimestamp(type, proposedDate);
		else {
			this._updateTimestamp(type, now);
			AlertIOS.alert('Oops!', 'You selected a future ' +type, [{text: 'OK'}])
		}
	},

	render: function() {
		var DateDisplay, chosenDate = this.state.chosenDate;

		if ( !_.isDate(chosenDate) || _.isEmpty(chosenDate.toString()) ) {
			DateDisplay =
				<View style={this._styles.dateDisplay}>
					<Text style={ [this._styles.ddText, {flex: 1}] }>
						-- No date and time selected -- 
					</Text>
				</View>			
		} else {
			DateDisplay =
				<View style={this._styles.dateDisplay}>
					<Text style={ [this._styles.ddText, {flex: 5}] }>
						{Moment(chosenDate).format("ddd, MMM Do, YYYY")}
					</Text>
					<Text style={ [this._styles.ddText, {flex: 2}] }>
						{Moment(chosenDate).format("h:mm")}
					</Text>
					<Text style={ [this._styles.ddText, {flex: 1}] }>
						{MERIDIEMS[this._meridiemIndex]}
					</Text>
				</View>
		}

		var boxStyle = {
			backgroundColor: "#000000",
			flexDirection: "column",
			height: Display.height - this.Dimensions.STATUS_BAR_HEIGHT,
			position: "absolute",
			top: this.Dimensions.STATUS_BAR_HEIGHT,
			width: Display.width,
		};

		return (
			<View style={boxStyle}>
				<ConvenienceButtons
					chosenDate={this.state.chosenDate}
					proposeDate={this._proposeDate} />
				<LineSeparator height={0} vertMargin={10} />
				{DateDisplay}
				<LineSeparator height={0} vertMargin={10} />
				<PickerSection
					chosenDate={this.state.chosenDate}
					meridiemIndex={this._meridiemIndex}
					priorDays={this._priorDays}
					proposeDate={this._proposeDate}
					proposeMeridiem={this._proposeMeridiem}
					proposeTime={this._proposeTime} />
				<SavePanel
					setItem={this._setTimestamp}
					trashItem={this._resetTimestamp} />
			</View>
		);
	}
});

/***********************************************************************************************
************************************************************************************************
																	P I C K E R    S E C T I O N

***********************************************************************************************/
var PickerSection = React.createClass({
	propTypes: {
		chosenDate: PropTypes.object,
		meridiemIndex: PropTypes.number,
		priorDays: PropTypes.array,
		proposeDate: PropTypes.func,
		proposeMeridiem: PropTypes.func,
		proposeTime: PropTypes.func,
  },

	_styles: StyleSheet.create({
		shell: {
			alignSelf: "center",
			backgroundColor: "#31B404",
			marginHorizontal: 4,
			width: Display.width,
		},
	}),

	_applyMeridiem: function(meridiem, hour) {
		if ( meridiem == "AM" && hour > 11 )
			hour -= 12;
		else if ( meridiem == "PM" && hour < 12)
			hour += 12;

		return hour;
	},

	_getChosenDateCopy: function() {
		return _.cloneDeep(this.props.chosenDate);
	},

	_prepDate: function(e) {
		var proposedDate = e.newValue;
		var chosenDate = this._getChosenDateCopy();
		var dateFormat = "M d YYYY";

		if ( Moment(chosenDate).format(dateFormat) == Moment(proposedDate).format(dateFormat) )
			return;

		this._submitTimestamp(proposedDate, "proposeDate");
	},

	_prepHour: function(e) {
		var newHour = parseInt(e.newValue);
		var chosenDate = this._getChosenDateCopy();
		
		if (chosenDate.getHours() == newHour)
			return;
		
		newHour = this._applyMeridiem(MERIDIEMS[this.props.meridiemIndex], newHour);

		chosenDate.setHours(newHour);
		this._submitTimestamp(chosenDate, "proposeTime");
	},

	_prepMeridiem: function(e) {
		var newMeridiem = e.newValue.toUpperCase();
		var chosenDate = this._getChosenDateCopy();
		var newHour = chosenDate.getHours();

		if (newMeridiem == MERIDIEMS[this.props.meridiemIndex])
			return;

		newHour = this._applyMeridiem(newMeridiem, newHour);
		chosenDate.setHours(newHour);
		this._submitTimestamp(chosenDate, "proposeMeridiem");
	},

	_prepMinute: function(e) {
		var newMinute = parseInt(e.newValue);
		var chosenDate = this._getChosenDateCopy();
		if (chosenDate.getMinutes() == newMinute)
			return;

		chosenDate.setMinutes(newMinute);
		this._submitTimestamp(chosenDate, "proposeTime");
	},

	_submitTimestamp: function(proposedTimestamp, method) {
		var proposedDate = Moment(proposedTimestamp).toDate();
		this.props[method](proposedDate);
	},

	render: function() {
		var chosenDate = _.isDate(this.props.chosenDate) ? this.props.chosenDate : Moment().toDate();
		var chosenHour = chosenDate.getHours();
		var index = {
			date: PRIOR_DAY_COUNT - Moment().startOf("day").diff( Moment(chosenDate).startOf("day"), "days" ) - 1,
			minute: _.indexOf(MINUTES, chosenDate.getMinutes()),
			hour: _.indexOf(HOURS, (chosenHour > 0 && chosenHour < 13) ? chosenHour : Math.abs(chosenHour - MAX_HOURS)),
			meridiem: this.props.meridiemIndex,
		}

		return (
			<PickerIOS style={this._styles.shell}>
        <PickerIOS.Group
        	key="date"
        	onChange={this._prepDate}
        	selectedIndex={index["date"]}
        	style={{flex: 3}}>
          {
          	this.props.priorDays.map((date, index) => {
          		return <PickerIOS.Item key={index} value={date} label={Moment(date).format("MM/DD").toString()} />
          	})
	        }
   			</PickerIOS.Group>
   			
   			<PickerIOS.Group
   				key="hour"
        	onChange={this._prepHour}
        	selectedIndex={index["hour"]}
        	style={{flex: 1}}>
          {
          	HOURS.map((hour, index) => {
          		return <PickerIOS.Item key={index} value={hour.toString()} label={hour.toString()} />
	        	})
	        }
   			</PickerIOS.Group>

   			<PickerIOS.Group
   				key="minute"
        	onChange={this._prepMinute}
        	selectedIndex={index["minute"]}
        	style={{flex: 1}}>
          {
          	MINUTES.map((minute, index) => {
          		var minString = ":" +((minute < 10) ? "0" : "") +minute.toString()
          		return <PickerIOS.Item key={index} value={minute.toString()} label={minString} />
	        	})
	        }
   			</PickerIOS.Group>
   			<PickerIOS.Group
   				key="meridiem"
        	onChange={this._prepMeridiem}
        	selectedIndex={index["meridiem"]}
        	style={{flex: 1}}>
          {
          	MERIDIEMS.map((meridiem, index) => (
          		<PickerIOS.Item key={index} value={meridiem} label={meridiem} />
	        	))
	        }
   			</PickerIOS.Group>
      </PickerIOS>
		);
	},
});


/***********************************************************************************************
************************************************************************************************
									C O N V E N I E N C E   B U T T O N S   S E C T I O N

***********************************************************************************************/
var ConvenienceButtons = React.createClass({
	propTypes: {
		chosenDate: PropTypes.object,
		proposeDate: PropTypes.func,
  },

	_styles: StyleSheet.create({
		shell: {
			flexDirection: "column",
			width: Display.width,
		},
		dateBtn: {
			borderRadius: 4,
			flex: 1,
			marginHorizontal: 4,			
			marginVertical: 4,
		}, dateBtnOn: {
			backgroundColor: "#DF7401"
		},dateBtnOff: {
			backgroundColor: "#2E2E2E",
			borderColor: "#DF7401",
			borderWidth: 1
		}, 
		dateBtnText: {
			fontFamily: "System",
			fontSize: 42,
			fontWeight: "100",
			letterSpacing: 5,
			// margin: 10,
			textAlign: "center",
		}, dateBtnTextOn: {
			color: "#000000",
		}, dateBtnTextOff: {
			color: "#DF7401",
		} 
	}),

	render: function() {
		var daysAgo = Moment().startOf("day").diff( Moment(this.props.chosenDate).startOf("day"), "days" );

		return (
			<View style={this._styles.shell}>
				<View style={ [this._styles.dateBtn, daysAgo == 0 ? this._styles.dateBtnOn : this._styles.dateBtnOff] }>
					<TouchableHighlight
						onPress={() => this.props.proposeDate(Moment().toDate())}
						underlayColor="#FE9A2E">
						<Text style={ [this._styles.dateBtnText, daysAgo == 0 ? this._styles.dateBtnTextOn : this._styles.dateBtnTextOff] }>Today</Text>
					</TouchableHighlight>
				</View>
				<View style={ [this._styles.dateBtn, daysAgo == 1 ? this._styles.dateBtnOn : this._styles.dateBtnOff] }>
					<TouchableHighlight
						onPress={() => this.props.proposeDate(Moment().subtract(1, 'day')._d)}
						underlayColor="#FE9A2E">
						<Text style={ [this._styles.dateBtnText, daysAgo == 1 ? this._styles.dateBtnTextOn : this._styles.dateBtnTextOff] }>Yesterday</Text>
					</TouchableHighlight>
				</View>
			</View>
		);
	},
});

module.exports = WhenScene;