// var StyleSheet = require("react-native").StyleSheet;

var Site = {
	_orgTypeIds: {
		CLIENT: "client",
		POLICE: "police",
		SECURITY: "security",
		VENDOR: "vendor"
	},
	_getAllyOrgTypeId: function(orgTypeId) {
		return (orgTypeId === "vendor") ? "client" : "vendor";
	},
	getSiteIdsByOrgType: function(siteRefs, orgType) {
		return _.chain(siteRefs).where({"isActive": true, "orgTypeId": orgType}).pluck("id").value();
	}
};

module.exports = Site;