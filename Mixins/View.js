// var Dims = require("react-native").Dimensions;
var StyleSheet = require("react-native").StyleSheet;

var View = {
	AspectRatios: {
		"21x9": 21/9,
		"16x9": 16/9,
		"16x10": 16/10,
		"4x3": 4/3,
		"1x1": 1
	},
	Colors: {
		night: {
			background: "#000000",
			block: "#2E2E2E",
			border: "#A4A4A4",
			separator: "#DF7401"
		},
		day: {
			background: "#FFFFFF",
			block: "#E6E6E6",
			border: "#424242",
			separator: "#DF7401"
		}
	},
	Dimensions: {
		ACTION_BTN_HEIGHT: 50,
		NAV_BAR_HEIGHT: 43,
		STATUS_BAR_HEIGHT: 20,
		TAB_BAR_HEIGHT: 50
	},
	Orientations: {
		// getOrientation: function() {
		// 	return Dims.get("window").width > Dims.get("window").height ? "landscapeRight" : "portrait"
		// },
		LANDSCAPE: "landscapeRight",
		PORTRAIT: "portrait"
	},
	Styles: {
		_textStyle: StyleSheet.create({
			need: {
				color: "#FA5858"
			},
			on: {
				color: "#3ADF00"
			},
			off: {
				color: "#A4A4A4"
			},
			"n/a": {
				color: "#585858"
			},
			liked: {
				color: "#31B404"
			},
			hated: {
				color: "#FF0000"
			}
		}),
		_viewStyle: StyleSheet.create({
			need: {
				color: "#FA5858"
			},
			on: {
				borderColor: "#3ADF00"
			},
			off: {
				borderColor: "#A4A4A4"
			},
			"n/a": {
				borderColor: "#1C1C1C"
			},
			liked: {
				backgroundColor: "#31B404"
			},
			hated: {
				backgroundColor: "#FF0000"
			}
		})
	}
};

module.exports = View;