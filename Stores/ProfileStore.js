'use strict'

var Reflux = require("reflux");
var Storage = require('react-native-store');

// STORES && ACTIONS
var HostActions = require("../Actions/HostActions");
var HostStore = require("./HostStore");
var LookupActions = require("../Actions/LookupActions");
var ProfileActions = require("../Actions/ProfileActions");
var RequestActions = require("../Actions/RequestActions");
var SiteActions = require("../Actions/SiteActions");
var UserActions = require("../Actions/UserActions");

// UTILITIES
var _ = require("lodash");

var ProfileStore = Reflux.createStore({
	_currentSiteRight: null,
	_currentUser: null,
	_db: null,
	_dbRefs: [],
	listenables: [ProfileActions],

	init: function() {
		this.listenTo(HostStore, this._updateDb, this._updateDb);
	},

	getInitialState: function() {
		return {
			currentSiteRight: this._currentSiteRight,
			currentUser: this._currentUser
		}
	},

	onChangePwd: function(email, oldPwd, newPwd) {
		this._db.changePassword({
		  email: email,
		  oldPassword: oldPwd,
		  newPassword: newPwd
		}, function(error) {
		  if (error) {
		    switch (error.code) {
		      case "INVALID_PASSWORD":
		        console.log("Wrong password, buddy");
		        break;
		      case "INVALID_USER":
		        console.log("Apparently you don't exist");
		        break;
		      default:
		        console.log("Error changing password:", error);
		    }
		  } else {
		    console.log("User password changed successfully!");
		  }
		});
	},

	onGetAuth: function() {
		var appName = "towmo";

		Storage.table(appName).then((table) => {
      var key = "auth";
      var authRow = table.where({
        "key": key
      }).find();

      if (authRow && authRow.length > 0) {
      	if (authRow[0].data == null)
      		ProfileActions.getAuth.failed();	
		  	else {
		  		var authData = JSON.parse(authRow[0].data);
		  		ProfileActions.getAuth.completed(authData);
		  	}
		  } else {
		  	ProfileActions.getAuth.failed();
		  }
		}).catch((err) => {
			console.log("A table for your app does not exist: ", appName);
			ProfileActions.getAuth.failed();
		});
	},

	onRemoveRequestId: function(userId) {
		var userStateRef = this._db.child(userId).child("state").child("requestId");

		userStateRef.set("", (err) => {
			if (err)
				ProfileActions.removeRequestId.failed(err);
			else {
				ProfileActions.removeRequestId.completed();
			}
		});
	},

	onSetCurrentUser: function(authData) {
		// var uid = this._parseUid(authData.uid);
		var uid = authData.uid;
		var userRef = this._db.orderByChild("uid").equalTo(uid);
		this._dbRefs.push(userRef);

		Storage.table("towmo").then((table) => {
      var key = "auth";
      var authRow = table.where({
        "key": key
      }).find();

      var authObj = {
	      "data": JSON.stringify(authData),
	      "key": key,
	    }

      if (!authRow || authRow.length == 0)
		  	table.add(authObj);
			else if (authRow[0].data == null)
      	table.update(authObj);
		});

		userRef.once("value", (result) => {
			var users = _.toArray(result.val());
			this._currentUser = users.length > 0 ? users[0] : null;
			
			// 1a. Check user's settings for preferred orgTypeId/SiteId combination
			var preferredSite = _.has(this._currentUser.settings, "preferredSite") ? this._currentUser.settings.preferredSite : undefined;
			
			this._currentSiteRight = this._getSiteRight(preferredSite);			
			this.trigger({
				currentUser: this._currentUser,
				currentSiteRight: this._currentSiteRight
			});

			ProfileActions.setCurrentUser.completed(this._currentUser);
		}, (err) => {
			console.log(err);
			return err;
		});

		userRef.on("child_changed", (result) => {
			this._currentUser = result.val();

			var preferredSite = _.has(this._currentUser.settings, "preferredSite") ? this._currentUser.settings.preferredSite : undefined;
			this._currentSiteRight = this._getSiteRight(preferredSite);

			this.trigger({
				currentUser: result.val(),
				currentSiteRight: this._currentSiteRight
			});
		}, (err) => {
			console.log(err);
			return err;
		});
		
		return userRef;
	},

	onSetFilter: function(states) {
		var params = "settings/filters/statuses/states";
		this._currentUser.settings.filters.statuses.states = states;
		this.trigger({
			currentUser: this._currentUser,
			currentSiteRight: this._currentSiteRight
		});

		var dbRef = this._db.child(this._currentUser.iid).child(params);
		
		dbRef.update(states, (err) => {
			if (err)
				ProfileActions.setFilter.failed();
			else
				ProfileActions.setFilter.completed();
		});
	},

	onSetRequestId: function(requestId, userId) {
		var userStateRef = this._db.child(userId).child("state").child("requestId");

		userStateRef.set(requestId, (err) => {
			if (err)
				ProfileActions.setRequestId.failed(err);
			else {
				ProfileActions.setRequestId.completed();
			}
		});
	},

	onSetPreferredSiteRight: function(siteRight) {
		var userId = this._currentUser.iid;
		var userRef = this._db.child(userId);

		userRef.child("settings").child("preferredSite").update({
			"id": siteRight.siteId,
			"orgTypeId": siteRight.orgTypeId
		}, (err) => {
			if (err)
				ProfileActions.setPreferredSiteRight.failed(err);
			else
				ProfileActions.setPreferredSiteRight.completed();
		});
	},

	onLogoutUser: function() {
		this._db.unauth();

		Storage.table("towmo").then((table) => {
      var key = "auth";
      var authRow = table.where({
        "key": key
      }).find();

      if (authRow && authRow.length > 0) {
		  	table.update({
		      "data": null,
		      "key": key,
		    });
      }
		});

		ProfileActions.logoutUser.completed();
		this.trigger({currentUser: this._currentUser = null});
		this._endAllListeners();
	},

	_endAllListeners: function() {
		_.each(this._dbRefs, (dbRef) => {
			dbRef.off("child_changed");
			dbRef.off("child_added");
		});

		LookupActions.endListeners();
		RequestActions.endListeners();
		SiteActions.endListeners();
		UserActions.endListeners();
	},

	_parseUid: function(iid) {
		return parseInt( iid.substr(iid.lastIndexOf(':') + 1) );
	},

	_getSiteRight: function(preferredSite) {
		var allSiteRights = this._currentUser.siteRights;
		
		// find suitable sight right
		var siteRight = _.findWhere(allSiteRights, {"siteId": preferredSite.siteId, "orgTypeId": preferredSite.orgTypeId})
		
		if (!siteRight) {
      // get 1st siteRight by default if no preferred site right was found
      siteRight = _.first(allSiteRights);

      if (!siteRight)
        return false;
      else {
        // Set default siteRight in Db
        ProfileActions.setPreferredSiteRight(siteRight);
      }
    }

    return siteRight;
	},

	_updateDb: function(data) {
		this._db = data.db.child("users");
	},
})


module.exports = ProfileStore;