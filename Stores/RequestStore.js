'use strict'

var Reflux = require("reflux");
var LookupStore = require("./LookupStore");
var NativeModules = require("react-native").NativeModules;
var HostActions = require("../Actions/HostActions");
var HostStore = require("./HostStore");
var LocationActions = require("../Actions/LocationActions");
var ProfileActions = require("../Actions/ProfileActions");
var ProfileStore = require("./ProfileStore");
var RequestActions = require("../Actions/RequestActions");
var SiteActions = require("../Actions/SiteActions");
var SiteStore = require("./SiteStore");

// MIXINS
var RequestMixin = require("../Mixins/Request");
var SiteMixin = require("../Mixins/Site");

// UTILITIES
var Async = require("async");
var Moment = require('moment');
var Defer = require("promise-defer");
var _ = require("lodash");

var RequestStore = Reflux.createStore({
	listenables: [RequestActions],
	mixins: [RequestMixin, SiteMixin],
	_currentUser: null,
	_currentSiteRight: null,
	_db: null,
	_dbRefs: [],
	_images: null,
	_imgTemplates: null,
	_lookups: null,
	_requests: new Array(2),
	_sites: null,
	_s3Policy: null,

	/*************************************************************************
		Currently, "all" = summary list, "user" = requests pertaining to single
		user.  Although, "site" would be more appropriate than "all".
	*************************************************************************/

	init: function() {
		this.listenTo(HostStore, this._updateHost, this._updateHost);
		this.listenTo(LookupStore, this._setLookups, this._setLookups);
		this.listenTo(ProfileStore, this._setProfile, this._setProfile);
		this.listenTo(SiteStore, this._setSites, this._setSites);
		
		this._requests["site"] = null;
		this._requests["user"] = null;
	},

	getInitialState: function() {
		return {
			requests: this._requests,
		}
	},

	_assignRequest: function(requestRef, entityType) {
		var requestId = requestRef.key();
		var request = requestRef.val();
		var existingRequests = this._requests[entityType]; // requests is NOT an array!!!
		
		if ( existingRequests[requestId] && !_.eq(existingRequests[requestId].statusEntries, request.statusEntries) )
			request.statusEntries = this._scrubStatusEntries(request.statusEntries, ["read", "status"]);

		existingRequests[requestId] = request;
		this._updateRequestList(existingRequests, entityType);
	},

	_filterRequests: function(requests) {
		/*
			Algorithm:
				1. Get most recent statusEntry
				2. check whether "nextStatuses" array exists
					a) If statusRef of any nextStatus has "write" status by orgType of current user
							then existing tow request is open
					b) otherwise requset is "closed"
		*/

		return _.filter(requests, (request) => {
			var includeRequest = false;

			if (_.isEmpty(request))
				return includeRequest;
			
			var siteRight = this._currentSiteRight;
			var lastStatusRef = this._lookups.statuses[_.last(request.statusEntries).statusId];
			var user = this._currentUser;
			var filterState = user.settings.filters.statuses.states
			
			if ( _.has(lastStatusRef, "nextStatuses") ) {
				var nextStatusIds = _.pluck(lastStatusRef.nextStatuses, "statusId");
					
				_.each(nextStatusIds, (statusId) => {
					var nextStatusRef = this._lookups.statuses[statusId];
					
					includeRequest = nextStatusRef.accessRights.read.status[siteRight.orgTypeId]
							? filterState[RequestMixin.Filters.OPEN]
							: filterState[RequestMixin.Filters.DONE]
				});
			} else
				includeRequest = filterState[RequestMixin.Filters.DONE]

			return includeRequest;
		});
	},

	_mapVehicleData: function(vehicleData) {
		var dataMap = this._lookups.hosts.vehicle["dataMap"];
    
    return {
  		drivetrain: dataMap.drivetrain[vehicleData.drivenWheels],
  		transmission: dataMap.transmission[vehicleData.transmission.transmissionType],
   		vehicleType: dataMap.vehicleType[vehicleData.categories.vehicleType],
  		make: vehicleData.make.name || "",
	    model: vehicleData.model.name || "",
	    year: vehicleData.years[0].year.toString() || 0
  	}
  },

  _scrubStatusEntries: function(statusEntries, params) {
    var currentOrgTypeId = this._currentSiteRight.orgTypeId
    var scrubbedStatusEntries = _.filter(statusEntries, (statusEntry) => {
      var statusRef = this._lookups.statuses[statusEntry.statusId];
      // var isStatusAllowed = (statusLookup.accessRights[currentOrgTypeId].status[action] === true);
      
      var isStatusAllowed = _.get(statusRef.accessRights, params)[currentOrgTypeId];

      return isStatusAllowed;
    });

		return scrubbedStatusEntries;
	},

  _sortRequests: function(requests) {
  	var now = Moment();

  	var sortedRequests = _.sortBy(requests, (request) => {
  		var statusEntry = _.last(request.statusEntries);
  		var diff = now.diff(Moment(statusEntry.timestamp), "seconds");
  		return diff;
  	});

  	return sortedRequests;
  },

  _updateRequestList: function(requests, entityType) {
  	// 1. Update model
  	if (!requests)
  		requests = this._requests[entityType];
  	else
  		this._requests[entityType] = requests;

  	var filteredRequests = {};

  	// 2. Filter requests based on selected filter
  	requests = this._filterRequests(_.toArray(requests));
  	
  	// 3. Sort requests based on their most recent status entry
  	requests = this._sortRequests(requests);
  	
  	_.each(requests, (request) => {
  		filteredRequests[request.iid] = request;
  	});
		
		this.trigger( {requests: filteredRequests} );
  },

  _urlForQuery: function(url, params) {
		var queryString = Object.keys(params).map(key => key + "=" +encodeURIComponent(params[key])).join("&");		
		return url +queryString;
	},

	onAddImgToHost: function(imgObj) {
		// 1. Get S3 Policy data for uploading
  	
  	/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  		Need to workout how to handle edge-case of expired s3Policy
  	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
		var fileExt = imgObj.file.ext;
		var s3Data = this._s3Policy.data;
		var s3Obj = {
			uri: imgObj.file.uri,
			uploadUrl: s3Data.url,
			mimeType: "image/" +fileExt,
			data: {
				acl: 'public-read',
				AWSAccessKeyId: s3Data.key,
				'Content-Type': "image/" +fileExt,
	      policy: s3Data.policy,
	      key: "requests/vehicle/" +imgObj.file.name,
	      signature: s3Data.signature,
      },
    };

  	NativeModules.FileTransfer.upload(s3Obj, (err, res) => {
    	if ( err == null && (res.status > 199 || res.status < 300) )
    		RequestActions.addImgToHost.completed();
    	else
    		RequestActions.addImgToHost.failed(err);
    });
	},

	onAddImgToDb: function(request, imgObj) {
		// 1. Get S3 Policy data for uploading
  	
  	/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  		Need to workout how to handle edge-case of expired s3Policy
  	!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
		var fileExt = imgObj.file.ext;
		var s3Data = this._s3Policy.data;
		var s3Obj = {
			uri: imgObj.file.uri,
			uploadUrl: s3Data.url,
			mimeType: "image/" +fileExt,
			data: {
				acl: 'public-read',
				AWSAccessKeyId: s3Data.key,
				'Content-Type': "image/" +fileExt,
	      policy: s3Data.policy,
	      key: "requests/vehicle/" +imgObj.file.name,
	      signature: s3Data.signature,
      },
    };

		Async.parallel([
  		(cb) => {
  			// 2a. upload image to Amazon S3
		    NativeModules.FileTransfer.upload(s3Obj, (err, res) => {
		    	if ( err == null && (res.status > 199 || res.status < 300) )
		    		cb(null, "Images loaded");
		    	else
		    		cb("Couldn't upload image", null);
		    });
  		}, (cb) => {
				var params = ["images", request.images.length];

				RequestActions.setParam(request, params, imgObj.dbRecord).then(() => {
					cb(null, "Db Successfully updated");
				}).catch((err) => {
					cb("Couldn't add new Img", null);
				});
  		}
  	], (err, results) => {
  		if (err)
  			RequestActions.addImgToDb.failed(err);
			else {
				RequestActions.addImgToDb.completed();
			}
  	});
	},

	onAddRequest: function(newRequest) {
		var requestsRef = this._db;
		var requestRef = requestsRef.push(newRequest);

		requestRef.update({"iid": requestRef.key()}, (err) => {
			if (err)
				RequestActions.addRequest.failed(err);
			else
				RequestActions.addRequest.completed(requestRef.key());
		});
	},

	onAddStatus: function(nextStatus, request, notes, sites) {
		console.log("requestId: ", request.iid);
		// var requestStatusEntriesRef = this._db.child(request.iid).child("statusEntries");
		var siteRight = this._currentSiteRight;
		var newUser = this._currentUser;
    
    LocationActions.getPosition.triggerPromise().then((position) => {
	    var prevUserId = _.last(request.statusEntries).author.id;
	    var statusEntry = {
	      timestamp: Moment(Moment().toDate()).format(),
	      geoPoint: position,
	      statusId: nextStatus.iid,
	      author: {
	        id: newUser.iid,
	        orgTypeId: siteRight.orgTypeId,
	      },
	      notes: notes,
	    };

			Async.parallel([
				(statusEntryCb) => {
					RequestActions.setParam.triggerPromise(request, ["statusEntries", request.statusEntries.length], statusEntry)
					.then(() => {
						// 1. assign request Id to user's current State when next status has islocked: true
	        	if (nextStatus.assignTo[siteRight.orgTypeId].user)
	            return ProfileActions.setRequestId(request.iid, newUser.iid);
	         	else {
	         		// need to also remove from user of previous status
	         		return ProfileActions.removeRequestId(prevUserId);
	         	}
					}).then(() => {
						statusEntryCb(null, "Status Entry has been saved");
					}).catch((err) => {
						statusEntryCb("Couldn't add status entry", null);
					});	
				},
				(siteAndRequestCb) => {
					// 2. Add request Id to all other allies' request list
		    	if ( nextStatus.assignTo[this._orgTypeIds.VENDOR].site ) {
		    		// iterate through all ally orgTypes of client
		    		var allySites = _.omit(sites, this._orgTypeIds.CLIENT)
		    			, qAllySites = [];

						_.each(allySites, (allySite, key) => {
							var qAllySite = new Promise((resolve, reject) => {
								Async.parallel([
									(requestIdCb) => {
										SiteActions.setRequestId.triggerPromise(request.iid, allySite.iid, key).then(() => {
											requestIdCb(null, "requestId added to " +key);
										}).catch((err) => {
											requestIdCb(err +": requestId NOT added to " +key, null);
										});
									},
									(siteIdCb) => {
										RequestActions.setParam.triggerPromise(request, ["sites", key, "siteId"], allySite.iid).then(() => {
											siteIdCb(null, allySite.iid +" added to request: " +request.iid);
										}).catch((err) => {
											siteIdCb(err +": " +allySite.iid +"NOT added to request: " +request.iid, null);
										});
									}
								], (err, results) => {
									if (err)
										reject();
									else
										resolve();
								});
							});

							qAllySites.push(qAllySite);
						});

						new Promise.all(qAllySites).then((results) => {
		      		siteAndRequestCb(null, "RequestId added to all Ally sites, and all siteIds added to request");
		      	}).catch((err) => {
	      			siteAndRequestCb(err +": Couldn't add requestId and/or SiteId", null);
	      		});
		    	} else
		    		siteAndRequestCb(null, "RequestId and SiteId unecessary");
				}
			], (err, results) => {
				if (err)
					RequestActions.addStatus.failed(err);

      	RequestActions.addStatus.completed();
			});
		});
	},

	onBuildImgObj: function(imgTypeId, imgUri) {
		// retrieve Amazon S3 policy parameters early enough
		/*************************************************************************
		 If S3 policy times out, error callback will have to obtain a new policy
		*************************************************************************/
		var beg = imgUri.lastIndexOf('/') +1
			, end = imgUri.lastIndexOf('.')
			, fileExt = imgUri.substr(end +1);
    var userId = this._currentUser.iid;
   	
   	LocationActions.getPosition.triggerPromise().then((position) => {
   		var geoPoint = {
   			lat: position.lat,
   			latitude: position.lat,
   			long: position.long,
   			longitude: position.long
   		};

   		var filename = this._buildImgFilename(Moment().format("X"),userId,imgTypeId,fileExt);  		
   		var stagedImg = {
	  		dbRecord: {
		      authorId: userId,
		      uri: this._images.folderpath +filename,
		      geoPoint: geoPoint,
		      imgTypeId: imgTypeId,
		      statusId: "",
		      timestamp: Moment( Moment().toDate() ).format(),
		    },
		    file: {
		      ext: fileExt,
		      name: filename,
		      uri: "" +imgUri,
		    },
			};

			RequestActions.buildImgObj.completed(stagedImg);	
   	});
	},

	onEndListeners: function() {
		_.each(this._dbRefs, (dbRef) => {
			dbRef.off("child_changed");
			dbRef.off("child_added");
		});
	},

	onExtractNextStatuses: function(statusEntry, requestId) {
		var self = this;
		var assignedToOtherRequest = function(requestId) {
      var assignmentStatus;
      var userState = self._currentUser.state;

      switch(self._currentSiteRight.orgTypeId) {
        case SiteMixin._orgTypeIds.VENDOR:
          assignmentStatus = (userState.requestId != "");
          break;
       	case SiteMixin._orgTypeIds.CLIENT:
          assignmentStatus = false;
          break;
      }

      return assignmentStatus;
    };

  	var assignedToThisRequest = function(requestId) {
      var assignmentStatus;
      var userState = self._currentUser.state;

      switch(self._currentSiteRight.orgTypeId) {
        case SiteMixin._orgTypeIds.VENDOR:
          assignmentStatus = (userState.requestId == requestId);
          break;
        
        case SiteMixin._orgTypeIds.CLIENT:
          assignmentStatus = true;
          break;
      }

      return assignmentStatus;
    };

    var checkUserRights = function(prevStatusRef, nextStatusRef) {
	    var statusRef = self._lookups.statuses[nextStatusRef.statusId];
	    var writeRight = statusRef.accessRights.write;
	    var taskRefs = self._lookups.tasks;
	    var allowed;
	    
	    if ( writeRight.status[self._currentSiteRight.orgTypeId] === true
	    	// && _.contains(self._currentSiteRight.tasks, RequestMixin.TaskIds.UPDATE_STATUS) ) {
				&& (writeRight.task === null || _.contains(self._currentSiteRight.tasks, writeRight.task)) ) {

	      // check lock status
	      if (prevStatusRef.lockForUser)
	        allowed = assignedToThisRequest(requestId);
	      else
	        allowed = assignedToOtherRequest(requestId) && _.has(statusRef, "nextStatuses") ? false : true;
	    } else {
	      allowed = false;
	    }

	    return allowed ? statusRef : undefined;
	  };

    var statusRef = this._lookups.statuses[statusEntry.statusId];
    var nextStatuses = null;

    if ( _.has(statusRef, "nextStatuses") ) {
    	nextStatuses = _.transform(statusRef.nextStatuses, (result, nextStatusRef) => {
      	var nextStatus = checkUserRights(statusRef, nextStatusRef);
      
	      if (nextStatus)
	        return result.push(nextStatus);
	    }) || new Array(0);

	    RequestActions.extractNextStatuses.completed(nextStatuses);
    } else
    	RequestActions.extractNextStatuses.completed(nextStatuses);
  },

  onGetRequest: function(requestId, ) {
  	var request = this._requests[requestId];
  	RequestActions.getRequest.completed(request);
  },

  onPullVehicleData: function(vin) {
  	var host = this._lookups.hosts["vehicle"];  	
		var url = host.url +vin +"?";
		var query = this._urlForQuery(url, host.params["url"]);

  	fetch(query, host.params["connection"])
			.then((res) => {
				if (res.status === 200) {
					var data = JSON.parse(res._bodyText);
					RequestActions.pullVehicleData.completed(this._mapVehicleData(data));
				} else {
					RequestActions.pullVehicleData.failed();	
				}
			}).catch((err) => {
				RequestActions.pullVehicleData.failed();
			});
  },

  onPullRequest: function(requestId, entityType) {
		if (!this._requests[entityType])
			return;

		var dbRef = this._db.child(requestId);
  	
  	dbRef.once("value", (requestRef) => {
			this._assignRequest(requestRef, entityType);
		});
  },

  // 1. One-time pull of existing tow request base
  // 2. Setup listener for any updates made to existing tow requests
	onPullRequests: function(requestIds, entityType) {
		var qRequests = Defer();
		var siteRight = this._currentSiteRight;
		var dbRef = this._db.orderByChild("sites/" +siteRight.orgTypeId +"/siteId").equalTo(siteRight.siteId);
		this._dbRefs.push(dbRef);
		
		dbRef.once("value", (allRequests) => {
			var requests = allRequests.val();
			var requestIds = _.keys(requests);
			/*
				Run sorting and filter functions
				1. Scrub Tow requests
				2. Filter requests
				3. Sort filtered requests
			*/

			if (!requestIds || requestIds.length === 0)
				requests = {};
			else
				_.each(requestIds, (requestId) => {
					var request = requests[requestId];
					request.statusEntries = this._scrubStatusEntries(request.statusEntries, ["read", "status"]);
					requests[requestId] = request;
				});

			this._updateRequestList(requests, entityType);
			RequestActions.pullRequests.completed(this._requests[entityType] = requests);
			qRequests.resolve();
		});

		// handle updated tow requests
		qRequests.promise.then(() => {
			dbRef.on("child_changed", (snapshot) => this._assignRequest(snapshot, entityType));
		});
	},

  onRefreshRequests: function(entityType) {
		this._updateRequestList(null, entityType);
  },

	onRemoveFromImages: function(request, imgTypeId) {
		var imagesRef = this._db.child(request.iid).child("images");

		imagesRef.transaction((prevList) => {
		  if (!prevList)
		  	return null;
		  else {
		  	var newList = _.remove(prevList, (image) => {
		  		return image.imgTypeId !== imgTypeId;
		  	});
		  	
		  	return newList;
		  }
		}, (err) => {
			if (err)
				RequestActions.removeFromImages.failed("Couldn't remove ${imgTypeId} img");
			else {
				RequestActions.setTodoStatus.triggerPromise(request, "images", "done", imgTypeId, false)
					.then(() => {
						RequestActions.removeFromImages.completed("Successfully removed ${imgTypeId} image");
					}).catch((err) => {
						RequestActions.removeFromImages.failed("Couldn't remove ${imgTypeId} img");
					});
				
			}
		});
	},

	onSetParam: function(request, params, value) {
		var ref = this._db.child(request.iid);
		var lastIndex = _.last(params);
		var action, arg;
		
		if (_.isNumber(lastIndex)) {
			action = "transaction";
			arg = function(prevList) {
				if (!prevList)
			  	prevList = [];

				prevList.push(value);
		    return prevList;
			};

			params = _.take(params, params.length - 1);
		} else {
			action = "set";
			arg = value;
		}

		_.each(params, (param) => {
			ref = ref.child(param);
		});

		ref[action](arg, (err) => {
			if (err)
				RequestActions.setParam.failed(err);
			
			var todoTriggerDef = this._lookups.todos.triggers[_.first(params)]
				, todoTrigger = todoTriggerDef ? this.getTodoTrigger(todoTriggerDef, action, value) : null;

			if (_.isEmpty(todoTrigger))
				RequestActions.setParam.completed();
			else
				RequestActions.setTodoStatus.triggerPromise(request, todoTrigger)
					.then(() => {
						RequestActions.setParam.completed();
					}).catch((err) => {
						RequestActions.setParam.failed();
					});
		});
	},

	onSetTodoStatus: function(request, trigger) {
		_.each(trigger.todos, (todoId) => {
			request.todoMap[todoId][trigger.state] = trigger.value;
		});
		
		RequestActions.setParam.triggerPromise(request, ["todoMap"], request.todoMap)
			.then(() => {
				RequestActions.setTodoStatus.completed();
			}).catch((err) => {
				RequestActions.setTodoStatus.failed(err);
			});
	},

	onUpdateVehicle: function(requestId, params, newValue) {
		var vehicleRef = this._db.child(requestId).child("vehicle");
		var paramRef = vehicleRef;

		_.each(params, (param) => {
			paramRef = paramRef.child(param);
		});

		paramRef.set(newValue, (err) => {
			if (err)
				RequestActions.updateVehicle.failed(err);
			else
				RequestActions.updateVehicle.completed();
		});
	},

	_buildImgFilename: function(timestamp,userId,imgTypeId,fileExt) {
 		return timestamp +"-" +userId +"-" +imgTypeId +"." +fileExt;
  },

	_setLookups: function(data) {
		this._lookups = data.lookups;
	},

	_setProfile: function(data) {
		this._currentUser = data.currentUser;
		this._currentSiteRight = data.currentSiteRight;
	},

	_setSites: function(data) {
		this._sites = data.sites;
	},

	_updateHost: function(data) {
		this._db = data.db.child("requests");
		this._images = data.images;
		this._s3Policy = data.s3Policy;
	},
});

module.exports = RequestStore;