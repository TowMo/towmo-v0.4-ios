'use strict'

var Reflux = require("reflux");
var HostStore = require("./HostStore");
var RequestActions = require("../Actions/RequestActions");
var SiteActions = require("../Actions/SiteActions");

// UTILITIES
var _ = require("lodash");

var LocationStore = Reflux.createStore({
	listenables: [SiteActions],
	_db: null,
	_dbRefs: [],
	_sites: new Array(4),

	init: function() {
		this.listenTo(HostStore, this._updateDb, this._updateDb);
		this._sites = {
			client: {},
			police: {},
			security: {},
			vendor: {}
		}
	},

	getInitialState: function() {
		return {
			sites: this._sites,
		}
	},

	onEndListeners: function() {
		_.each(this._dbRefs, (dbRef) => {
			dbRef.off("child_changed");
			dbRef.off("child_added");
		});
	},

	onGetEmployerSite: function(orgTypeId, siteId) {
		SiteActions.getEmployerSite.completed(this._sites[orgTypeId][siteId]);
	},

	onGetSite: function(siteId, orgTypeId) {
		var site = _.findWhere(this._sites[orgTypeId], {"iid": siteId});
		SiteActions.getSite.completed(site);
	},

	onPullEmployerSite: function(currentSiteId, employerOrgTypeId) {
		var dbRef = this._db.child(employerOrgTypeId).child(currentSiteId);
		this._dbRefs.push(dbRef);
		
		dbRef.once("value", (employerSite) => {
			this._sites[employerOrgTypeId][currentSiteId] = employerSite.val();
			this.trigger({sites: this._sites});
			SiteActions.pullEmployerSite.completed(employerSite.val());
		});

		dbRef.on("child_changed", (paramRef) => {
			var employerSite = this._sites[employerOrgTypeId][currentSiteId];
			employerSite[paramRef.key()] = paramRef.val();
		});

		// update currently request list when employer request list updates
		var requestsRef = dbRef.child("requests");
		this._dbRefs.push(requestsRef);
		
		requestsRef.on("child_added", (requestRef) => {
			var newRequestId = requestRef.val();
			RequestActions.pullRequest(newRequestId, "site");
		})
	},

	onPullAllySites: function(allyOrgTypeId, allySiteIds) {
		var dbRef = this._db.child(allyOrgTypeId);
		this._dbRefs.push(dbRef);

		dbRef.once("value", (results) => {
			_.each(allySiteIds, (siteId) => {
				this._sites[allyOrgTypeId][siteId] = results.val()[siteId];
			});
			
			this.trigger({sites: this._sites});
			SiteActions.pullAllySites.completed(this._sites[allyOrgTypeId]);
		}, (err) => {
			SiteActions.pullAllySites.failed(err);
		});

		dbRef.on("child_changed", (paramRef) => {
			var allySiteId = paramRef.key();
			this._sites[allyOrgTypeId][allySiteId] = paramRef.val();
		});
	},

	onSetRequestId: function(requestId, siteId, orgTypeId) {
		var siteRequestsRef = this._db.child(orgTypeId).child(siteId).child("requests");
		this._dbRefs.push(siteRequestsRef);

		siteRequestsRef.transaction((prevList) => {
		  /******************************************************************
				Note: prevList represents the requests array.  Requests will be
				null in the event no requestIds exist; therefore special actions
				need to be taken in order to add the "requests" parameter before
				populating
		  ******************************************************************/
		  if (prevList === null)
		  	return "";
		  else {
		  	if (prevList === "")
		  		return [requestId];
		  	else {
		  		if ( !_.contains(prevList, requestId) )
		  			prevList.push(requestId);

		  		return prevList;
		  	}
		  }
		}, (err, commit, snapshot) => {
			if (err || !commit)
				SiteActions.setRequestId.failed(err);
			// else if (!snapshot.val())
			else
				SiteActions.setRequestId.completed();
		});
	},

	_updateDb: function(data) {
		this._db = data.db.child("sites");
	},
});


module.exports = LocationStore;