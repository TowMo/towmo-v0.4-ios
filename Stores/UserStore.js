'use strict'

var Reflux = require("reflux");
var HostStore = require("./HostStore");
var UserActions = require("../Actions/UserActions");

// UTILITIES
var _ = require("lodash");

var UserStore = Reflux.createStore({
	listenables: [UserActions],
	_db: null,
	_users: new Array(2),

	init: function() {
		this.listenTo(HostStore, this._updateDb, this._updateDb);
		
		this._users["site"] = new Array(2);
		this._users["site"]["vendor"] = {};
		this._users["site"]["client"] = {};
		this._users["user"] = null;
	},

	getInitialState: function() {
		return {
			users: this._users,
		}
	},

	onEndListeners: function() {

	},

	onPullUsers: function(orgTypeId, entityType, userIds) {
		var dbRef = this._db;

		if (_.isEmpty(userIds))
			UserActions.pullUsers.completed();
		else
			dbRef.once("value", (allUsersRef) => {
				var allUsers = allUsersRef.val();
				var users = {};
				_.each(userIds, (key) => {
					users[key] = allUsers[key];
				});

				this._users[entityType][orgTypeId] = users;
				UserActions.pullUsers.completed(users);
				this.trigger({users: this._users});
			});
	},

	_updateDb: function(data) {
		this._db = data.db.child("users");
	},
});

module.exports = UserStore;