'use strict';

// REACT NATIVE PARTS
var React = require("react-native");
var Icons = require("react-native-vector-icons/Ionicons");
var Reflux = require("reflux");

// PERSONAL COMPONENTS
var LoginScene = require("./LoginScene");
// var RegisterScene = require("../Scenes/RegisterScene");

// ACTIONS && HOSTS
var HostStore = require("../Stores/HostStore");
var HostActions = require("../Actions/HostActions");

var {
  PropTypes,
	StyleSheet,
	Text,
	View,
} = React;

var styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    flex: 1
  },
});

var AuthMain = React.createClass({
	mixins: [Reflux.connect(HostStore), Reflux.ListenerMixin],
  propTypes: {
    initSession: PropTypes.func,
    setInProgress: PropTypes.func
  },
  getDefaultProps: function() {
    return {
      resolveDependencies: null,
    }
  },

  getInitialState: function() {
    return {
      preferredScene: "login",
    };
  },

  componentWillMount: function() {
    console.log("Auth Context Mounted");
  },

  componentWillUnmount: function() {
    console.log("Auth Context unMounted");
  },

  render: function() {
    if (this.state.preferredScene === "login")
      return (
        <LoginScene
          {...this.props}
          db={this.state.db} />
      );
    else
      return <RegisterScene {...this.props} />;
  },
});

module.exports = AuthMain;