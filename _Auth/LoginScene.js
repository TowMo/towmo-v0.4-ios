'use strict';

var Accordion = require("react-native-collapsible/Accordion");
var Display = require("react-native-device-display");
var React = require("react-native");
var Icon = require("react-native-vector-icons/Ionicons");
var Refresh = require("react-native-refreshable-listview");

// ACTIONS && HOSTS
var LookupActions = require("../Actions/LookupActions");
var ProfileActions = require("../Actions/ProfileActions");

// COMPONENTS
var LineSeparator = require("../Comps/LineSeparator");

// MIXINS
var SiteMixin = require("../Mixins/Site");
var ViewMixin = require("../Mixins/View");

// UTILS
var _ = require("lodash");

var {
	Image,
	ListView,
	PropTypes,
	StatusBarIOS,
	StyleSheet,
	Text,
	TextInput,
	TouchableHighlight,
	View,
} = React;

var styles = StyleSheet.create({
	main: {
		flexDirection: "column",
		height: Display.height,
		position: "absolute",
		top: ViewMixin.Dimensions.STATUS_BAR_HEIGHT,
		width: Display.width
	}, orgTypeBox: {
			flex: 1,
			flexDirection: "row",
		}, orgTypeBtn: {
			alignItems: "center",
			borderRadius: 3,
			borderWidth: 1,
			flex: 1,
			margin: 6
		},
		sitesBox: {
			flex: 4,
		}, siteBox: {
				flexDirection: "row",
				margin: 6
			}, siteImgBox: {
					flex: 1,
				}, siteImg: {
						borderRadius: 4,
						flex: 1,
						justifyContent: "center"
					}, 
				siteTextBox: {
					flex: 5,
					justifyContent: "center",
					padding: 6
				}, siteText: {
						color: "#FFFFFF",
						fontSize: 24,
						fontWeight: "200",
						justifyContent: "center",
						textAlign: "left"
					},
		usersBox: {
			justifyContent: "center",
			flexDirection: "row",
			flexWrap: "wrap"
		}, userBox: {
				borderWidth: 0.75,
				margin: 6,
				width: (Display.width - 3 * 12) / 3
			}, userImg: {
				resizeMode: "cover",
				height: (Display.width - 3 * 12) / 3
			}, userTextBox: {
				padding: 2
			}, userText: {
					color: "#000000",
					fontSize: 22,
					fontWeight: "200",
					textAlign: "center"
				},
			alliesBox: {

			}
});

var LoginScene = React.createClass({
	mixins: [SiteMixin, ViewMixin],
	propTypes: {
		db: PropTypes.object,
		initSession: PropTypes.func,
		setInProgress: PropTypes.func
	},
	getInitialState: function() {
		return {
			accordion: null,
			ds: new ListView.DataSource({rowHasChanged: (r1, r2) => r1.guid !== r2.guid}),
			lookups: null,
			orgTypeId: "vendor",
			siteId: null,
			sites: null,
			userId: null,
			users: null
		}
	},

	componentWillMount: function() {
		console.log("Login Scene Mounted");
		var qLookups = LookupActions.validateLookups.triggerPromise().then((lookups) => {
      return {
      	key: "lookups",
      	data: lookups
      };
    });
		
		var qSites = this._reloadTable("sites")
			, qUsers = this._reloadTable("users");

		Promise.all([qLookups, qUsers, qSites]).then((results) => {
			_.each(results, (table) => {
				var tableData = table.data;
				var tableName = table.key;
				
				if (tableName !== "sites")
					this.state[tableName] = tableData;
				else {
					var lookups = this.state.lookups;
					var imgHostUrl = lookups.hosts.images.url;
					var chosenOrgTypeId = this.state.orgTypeId;
					var sites = this.state.sites = new Array( _.toArray(tableData).length );
					var userDims = {
						height: (Display.width - 3 * 12) / 3,
						width: (Display.width - 3 * 12) / 3
					};

					_.each(tableData, (sitesData, orgTypeId) => {
						sitesData = _.where(sitesData, {"isActive": true});
						
						sites[orgTypeId] = {
							accordion: this._resetAccordion( _.keys(sitesData) ),
							data: _.map(sitesData, (site, siteId) => {
								var orgTypes = lookups.orgTypes;
								var userIds = _.pluck( _.where(site.users, {"isActive": true}), "id");
								var users = [];
							
								_.each(userIds, (userId) => {
									users.push(this.state.users[userId])
								});

								return {
									"header": (
										<View key={siteId} style={styles.siteBox}>
											<View style={styles.siteImgBox}>
												<Image
													source={{ uri: imgHostUrl +site.uri +"?fit=crop&w=49&h=49"}}
													style={{height: 49, width: 49}} />
											</View>
											<View style={styles.siteTextBox}>
												<Text numberOfLines={1} style={styles.siteText}>{site.name}</Text>
											</View>
										</View>
									),
									"content": (
										<View style={styles.usersBox}>{
											_.map(users, (user) => (
												<TouchableHighlight
													key={user.iid}
													onPress={() => this._processLogin(user.email)}>
													<View style={ [styles.userBox, {borderColor: orgTypes[orgTypeId].color}] }>
														<Image
															source={{uri: imgHostUrl +user.uri.selfie +"?fit=crop&w=" +userDims.width +"&h=" +userDims.height}}
															style={styles.userImg} />
														<View style={ [styles.userTextBox, {backgroundColor: orgTypes[orgTypeId].color}] }>
															<Text style={styles.userText}>{user.name.first}</Text>
														</View>
													</View>
												</TouchableHighlight>
											))
										}
										</View>
									),
									"siteId": siteId
								}
							})
						};
					});
				}	
			});

			this.setState(this.state);
		});
	},

	componentWillUnmount: function() {
		console.log("Login Scene unmounted");
	},

	_processLogin: function(email) {
		var creds = {
			email: email,
			password: "test"
		};
		
		this.props.setInProgress(true);
		this.props.db.authWithPassword(creds, (err, authData) => {
			if (authData) {
				ProfileActions.setCurrentUser.triggerPromise(authData).then((user) => {
					this.props.initSession();
	      }).catch((err) => {
	        /*
	        err doesn't necessarily mean user wasn't logged in.
	        Look at using AsyncStorage for user
	        */
	      	console.log("AWC: Something went wrong: ", err);  
	      });
			} else {
				console.log("Error logging in...");
			}
		});
	},

	_reloadTable: function(table) {
		var tableRef = this.props.db.child(table);
		
		return new Promise((resolve, reject) => {
			tableRef.once("value", (tableData) => {
				resolve({
					key: tableData.key(),
					data: tableData.val()
				});
			});
		});
	},

	// _updateCreds: function(param, newText) {
	// 	this.state.creds[param] = newText;
	// 	this.setState(this.state);
	// },

	_renderContent: function(section) {
		return section.content;
	},

	_renderHeader: function(section) {
		return section.header;
	},

	_renderSeparator: function(section, requestId) {
		// var requests = _.toArray(this.props.requests);
		return (
			<LineSeparator color="#01A9DB" height={0.75} vertMargin={20} />
		);
	},

	_renderSites: function(sites, sectionId, rowId) {
		if (rowId !== this.state.orgTypeId)
			return null;

		return (
			<Accordion
				key={rowId}
				onChange={(index) => this._updateAccordion(index, sites)}
				renderHeader={this._renderHeader}
				renderContent={this._renderContent}
				sections={sites.data} />
		);
	},

	_resetAccordion: function(obj) {
		return _.transform(obj, (result, value, key, remaining) => {
			if (typeof value == "object")
				result[value] = this._resetAccordion(value);
			else
				result[value] = false;
		});
	},

	_updateAccordion: function(index, sites) {
		var siteIds = _.pluck(sites.data, "siteId");
		var pressedSiteId = siteIds[index];
		var pressedValue = sites.accordion[pressedSiteId];
		
		this.state.sites[this.state.orgTypeId].accordion = this._resetAccordion(siteIds);
		this.state.sites[this.state.orgTypeId].accordion[pressedSiteId] = !pressedValue;
		this.setState(this.state);
	},

	render: function() {
		return (
			<View style={styles.main}>
				<View style={styles.orgTypeBox}>
					<TouchableHighlight
						onPress={() => this.setState({orgTypeId: this._orgTypeIds.CLIENT})}
						style={ [styles.orgTypeBtn, (this.state.orgTypeId === this._orgTypeIds.CLIENT) ? {backgroundColor: "#DF7401"} : {backgroundColor: "#848484"}] }>
						<Image
							source={require('image!bldg')} />
					</TouchableHighlight>
					<TouchableHighlight
						onPress={() => this.setState({orgTypeId: this._orgTypeIds.VENDOR})}
						style={ [styles.orgTypeBtn, (this.state.orgTypeId === this._orgTypeIds.VENDOR) ? {backgroundColor: "#01A9DB"} : {backgroundColor: "#848484"}] }>
						<Image
							source={require('image!truck')}
							style={styles.siteImg} />
					</TouchableHighlight>
				</View>
				{this.state.sites ?
					<Refresh
						contentInset={{top: -this.Dimensions.STATUS_BAR_HEIGHT}}
		        dataSource={this.state.ds.cloneWithRows(this.state.sites)}
		        removeClippedSubviews={true}
		        renderRow={this._renderSites}
		        style={styles.sitesBox}
						loadData={() => this._reloadTable("sites")} />
					: <View style={styles.sitesBox}><Text>Getting Data...</Text></View>
				}
			</View>
		);
	}
})

module.exports = LoginScene;