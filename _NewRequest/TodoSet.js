'use strict';

// REACT PARTS
var Display = require('react-native-device-display');
var Icon = require('react-native-vector-icons/Ionicons');
var React = require("react-native");
var TextInputState = require('TextInputState');

// CUSTOM COMPONENTS
// var Site = require("../Comps/Site");
var WhenMgr = require("../Comps/WhenMgr");

// MIXINS
var RequestMixin = require("../Mixins/Request");
var SiteMixin = require("../Mixins/Site");
var ViewMixin = require("../Mixins/View");

// Utilities
var Moment = require("moment");
var _ = require("lodash");

var {
	Image,
	ListView,
	Modal,
	PropTypes,
	StatusBarIOS,
	StyleSheet,
	SwitchIOS,
	Text,
	TextInput,
	TouchableHighlight,
	View,
} = React;

var TodoSet = React.createClass({
	mixins: [RequestMixin, SiteMixin, ViewMixin],
	propTypes: {
		editable: PropTypes.bool,
		moveInput: PropTypes.func,
		orgType: PropTypes.object,
		setTodo: PropTypes.func,
		siteRight: PropTypes.object,
		todoEntrySet: PropTypes.object,
		todoSet: PropTypes.object
  },

  getInitialState: function() {
  	return {
  		showWhenMgr: false
  	}
  },

	_styles: StyleSheet.create({
		todoItemHorz: {
			flex: 1,
			flexDirection: "row",
			marginHorizontal: 2,
			padding: 6
		}, todoTitleHorz: {
				flex: 5
			},
			todoValueHorz: {
				flex: 1,
				justifyContent: "center"
			},
			
		todoItemVert: {
			flex: 1,
			flexDirection: "column",
			paddingVertical: 4
		}, 
			todoTitleVert: {
				borderColor: "#A4A4A4",
				flex: 1,
				marginHorizontal: 2,
				paddingBottom: 2,
				paddingHorizontal: 6
			},
			todoValueVert: {
				flex: 1,
				marginHorizontal: 2,
				paddingHorizontal: 6,
				paddingVertical: 2
			},
				
		ttText: {
			color: "#A4A4A4",
			fontFamily: "System",
			fontSize: 19,
			fontWeight: "200"
		},
		tvText: {
			fontFamily: "System",
			fontWeight: "200",
			letterSpacing: 1
		}
	}),

	componentWillMount: function() {
		StatusBarIOS.setStyle("light-content");
	},

  // Scroll a component into view. Just pass the component ref string.
	_moveInput: function(target, offset) {
		this.props.moveInput(this.refs[target], offset);
	},

	_setTodoItem: function(todoId, newValue) {
		var todoEntrySet = _.cloneDeep(this.props.todoEntrySet);
		todoEntrySet[todoId].value = newValue;
		this.props.setTodo(todoEntrySet, [this.props.orgType.iid], this.isDone( _.pluck(_.toArray(todoEntrySet), "value") ));
	},

	_toggleShowWhenMgr: function(state) {
		this.setState({showWhenMgr: state});
	},

	_renderItem: function(todoItem) {		
		var ViewText, TodoView
			, TodoTitle = <Text style={this._styles.ttText}>{todoItem.title}</Text>
			, props = this.props
			, viewStyle = this._styles.todoItemVert
			, titleStyle = this._styles.todoTitleVert
			, editable = props.editable || (_.has(todoItem, "accessRights") && _.contains(todoItem.accessRights.write, props.siteRight.orgTypeId))
			, todoEntry = props.todoEntrySet[todoItem.iid]
		
		var color = this.isDone([todoEntry.value]) ? props.orgType.color : this.Colors.night.border;

		switch(todoItem.inputWay) {
			case "input":
				let inputStyle = {
					color: color, 
					borderColor: color, 
					borderRadius: 2, 
					borderWidth: 0.5,
					fontSize: todoItem.fontSize, 
					height: 50
				};

				ViewText = todoEntry.value || "-- No info provided yet --";
				TodoView = 
					<TextInput
				  	onChangeText={(value) => this._setTodoItem(todoItem.iid, value)}
				  	onFocus={() => this._moveInput(todoItem.iid, 110)}
						maxLength={140}
						multiline={true}
						ref={todoItem.iid}
						style={ [this._styles.todoValueVert, this._styles.tvText, inputStyle] }
						value={todoEntry.value} />

				titleStyle = this.props ? titleStyle : [titleStyle, {borderBottomWidth: 0.75}];
				break;

			case "switch":
				ViewText = todoEntry.value ? "YES" : "NO" || "-- No info provided yet --";
				TodoView =
					<SwitchIOS
						disabled={!editable}
						onTintColor={props.orgType.color}
	          onValueChange={(newValue) => this._setTodoItem(todoItem.iid, newValue)}
	          style={this._styles.todoValueHorz}
	          value={todoEntry.value} />

				viewStyle = this._styles.todoItemHorz;
				titleStyle = this._styles.todoTitleHorz;
				break;

			case "timestamp":
				let timestampStyle = {
					color: color,
					fontSize: todoItem.fontSize
				};

				ViewText = _.isEmpty(todoEntry.value) ? "-- No info provided yet --" : Moment(todoEntry.value).format("ddd MMM Do, YYYY @h:mm a");
				TodoView = this.state.showWhenMgr ?
					<Modal
						animation={false}
						visible={this.state.showWhenMgr}>
						<WhenMgr
							closeDisplay={() => this._toggleShowWhenMgr(false)}
							initialVal={todoEntry.value}
							lookups={props.lookups}
							setDate={(newTimestamp) => this._setTodoItem(todoEntry.todoId, newTimestamp)}
							statusBarHeight={this.Dimensions.STATUS_BAR_HEIGHT} />
		    	</Modal> :
					<TouchableHighlight
						onPress={() => this._toggleShowWhenMgr(true)}>
						<View style={ [viewStyle, this._styles.todoValueVert] }>
							<Text style={ [this._styles.tvText, timestampStyle] }>{ViewText}</Text>
						</View>
					</TouchableHighlight>

				break;
		}
		
		if (!editable)
			TodoView = 
				<View style={ [viewStyle, {borderColor: color}] }>
					<Text style={ [this._styles.tvText, {color: color}] }>{ViewText}</Text>
				</View>

		return (
			<View key={todoItem.iid} style={viewStyle}>
				<View style={titleStyle}>{TodoTitle}</View>
				{TodoView}
			</View>
		);
	},

	render: function() {
		if ( _.isEmpty(this.props.todoSet) || _.isEmpty(this.props.todoEntrySet) )
			return null;
		else
			return (
				<View>{
		      _.map(this.props.todoSet, (todoItem) => {
	      		return this._renderItem(todoItem)
		      })
				}
				</View>
			);
	},
});

module.exports = TodoSet;