'use strict';

// REACT PARTS
var React = require("react-native");
var Reflux = require("reflux");

// COMPONENTS
var LineSeparator = require("../Comps/LineSeparator");
var StatusEntry = require("../Comps/StatusEntry");

// COMPONENTS
var SiteMixin = require("../Mixins/Site");
var ViewMixin = require("../Mixins/View");

// ACTIONS && STORES
var SiteStore = require("../Stores/SiteStore");

// Utilities
var _ = require("lodash");

var {
	Image,
	ListView,	
	PropTypes,
	StyleSheet,
	Text,
	View,
} = React;

var _styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: "column",
		paddingHorizontal: 12,
		paddingVertical: 4
	},
 	rowBox: {
		flex: 1,
		flexDirection: "row",
	},
	statusEntry: {
		flex: 3,
		flexDirection: "column"
	}, statusBox: {
			flexDirection: "row"
		}, status: {
				fontSize: 28,
				fontFamily: "helvetica neue",
				fontWeight: "200",
			}, liked: {
				color: "#01DF01",
			}, disliked: {
				color: "#FF0000"
			}, icon: {
				fontSize: 22,
				justifyContent: "center",
				paddingHorizontal: 6
			}, active: {
				color: "#01DF01"
			}, inActive: {
				color: "#424242"
			},
		author: {
			color: "#A4A4A4",
			fontSize: 22,
			fontFamily: "helvetica neue",
			fontWeight: "200",
		},
		date: {
			fontSize: 16,
			color: "#DF7401"
		},
	img: {
		flex: 1
	}
});

var HistoryScene = React.createClass({
	mixins: [Reflux.ListenerMixin, Reflux.connect(SiteStore), SiteMixin, ViewMixin],
	propTypes: {
		currentSiteRight: PropTypes.object,
		currentUser: PropTypes.object,
		ds: PropTypes.object,
		lookups: PropTypes.object,
		request: PropTypes.object,
		sites: PropTypes.object,
		statusLookups: PropTypes.object,
		themeColor: PropTypes.string,
		users: PropTypes.array
	},

	getInitialState: function() {
		return {
			imgDims: null
		};
	},

	_renderRow: function(statusEntry, sectionId, rowId, themeColors) {
		let props = this.props
			, statusRef = props.statusLookups[statusEntry.statusId]
			, statusEntryStyle = StyleSheet.create({
				mainBox: {
					flex: 3,
					flexDirection: "column"
				}, status: {
						flex: 1,
						fontSize: 28,
						fontFamily: "helvetica neue",
						fontWeight: "200"
					}, author: {
						color: themeColors[statusEntry.author.orgTypeId],
						fontSize: 22,
						fontFamily: "helvetica neue",
						fontWeight: "200",
					}, date: {
						flex: 1,
						fontSize: 16,
						color: "#A4A4A4"
					},
				img: {
					flex: 1
				}
			});

		if (statusRef.accessRights.read.status[this.props.currentSiteRight.orgTypeId])
			return (
				<StatusEntry
					key={statusEntry.statusId}
					currentUser={props.currentUser}
        	currentSiteRight={props.currentSiteRight}
					lookups={props.lookups}
					request={props.request}
					show={{author: true, timestamp: true, img: true, status: true}}
					sites={props.sites}
					statusEntry={statusEntry}
					styles={statusEntryStyle}
					themeColors={props.themeColors}
					users={props.users} />
			);
		else
			return null;
	},

	_renderSeparator: function(status, rowId) {
		if ( parseInt(rowId) < (this.props.request.statusEntries.length - 1) )
			return (<LineSeparator key={rowId} height={0.5} horzMargin={0} vertMargin={10} />);
	},

	render: function() {
		let props = this.props, state = this.state
			, statusEntries = _.cloneDeep(props.request.statusEntries)
			, themeColors = {};
		
		themeColors[this._orgTypeIds.CLIENT] = props.lookups.orgTypes[this._orgTypeIds.CLIENT].color;
		themeColors[this._orgTypeIds.VENDOR] = props.lookups.orgTypes[this._orgTypeIds.VENDOR].color;

		return (
			<ListView
				contentInset={{top: -this.Dimensions.STATUS_BAR_HEIGHT}}
				dataSource={props.ds.cloneWithRows(statusEntries)}
				initialListSize={statusEntries.length}
				renderRow={(statusEntry, sectionId, rowId) => this._renderRow(statusEntry, sectionId, rowId, themeColors)}
				renderSeparator={this._renderSeparator}
				style={_styles.container} />
		);
	},
});

module.exports = HistoryScene;