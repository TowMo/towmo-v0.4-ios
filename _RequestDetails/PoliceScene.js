'use strict';

// REACT PARTS
var Display = require("react-native-device-display");
var Icon = require('react-native-vector-icons/Ionicons');
var React = require("react-native");
var Reflux = require("reflux");

// COMPONENTS
var ActionButtons = require("../Comps/ActionButtons");
var LineSeparator = require("../Comps/LineSeparator");
var Pending = require("../Comps/Pending");
var Site = require("../Comps/Site");
var StatusEntry = require("../Comps/StatusEntry");

// MIXINS
var RequestMixin = require("../Mixins/Request");
var SiteMixin = require("../Mixins/Site");
var ViewMixin = require("../Mixins/View");

// ACTIONS && STORES
var RequestActions = require("../Actions/RequestActions");
var SiteStore = require("../Stores/SiteStore");

// Utilities
var _ = require("lodash");

var {
	Image,
	ListView,	
	Modal,
	PropTypes,
	StyleSheet,
	Text,
	TextInput,
	TouchableHighlight,
	View,
} = React;

var _styles = StyleSheet.create({
	main: {
		flex: 1,
		flexDirection: "column",
	}, section: {
		flexDirection: "row",
		justifyContent: "center",
		paddingHorizontal: 4,
		paddingVertical: 14
	},
	caseNumberTitleBox: {
		flex: 3
	}, caseNumberTitleText: {
			fontSize: 24,
			fontFamily: "System",
			fontWeight: "200",
			padding: 4,
			textAlign: "right"
		},
		caseNumberValueBox: {
			alignSelf: "center",
			flex: 7,
			justifyContent: 'center',
		},
		caseNumberInputBox: {
			backgroundColor: '#1C1C1C',
			borderWidth: 0.75,
			height: 40,
		}, caseNumberText: {
				fontSize: 22,
				fontFamily: "System",
				fontWeight: "200",
				letterSpacing: 2,
				paddingHorizontal: 10,
				textAlign: "left",
			},

	callBtn: {
		alignSelf: "center",
		alignItems: "center",
		borderRadius: 6,
		flex: 2,
		justifyContent: "center",
		paddingHorizontal: 8,
		position: "absolute",
		right: 2
	}, callBtnIcon: {
		color: "#FFFFFF",
		fontSize: 65,
		justifyContent: "center",
		textAlign: "center"
	},
	site: {
		flex: 1,
		flexDirection: "row"
	},
	actionButtons: {
		alignItems: "center",
		bottom: 50,
		flexDirection: "row",
		position: "absolute",
		width: Display.width - 5
	},
	submitting: {
	  height: Display.height,
	  justifyContent: "center",
	  width: Display.width,
	}
});

var PoliceScene = React.createClass({
	mixins: [RequestMixin, SiteMixin, ViewMixin],
	propTypes: {
		currentSiteRight: PropTypes.object,
		lookups: PropTypes.object,
		request: PropTypes.object,
		sites: PropTypes.object,
		themeColors: PropTypes.object
	},

	_currentWorkflow: "save",
	getInitialState: function() {
		return {
			caseNumber: this.props.request.caseNumber,
			chosenStatus: null,
			finalize: false,
			workflowStages: {
				save: [
	        {
	          isActive: true,
	          msg: "Waiting to save",
	          end: false,
	          success: true
	        }, {
	          isActive: false,
	          msg: "Saving...",
	          end: false,
	          success: true
	        }, {
	          isActive: false,
	          msg: "New Case # Updated!",
	          end: true,
	          success: true
	        }, {
	          isActive: false,
	          msg: "No save: something wrong!",
	          end: true,
	          success: false
	        }
	      ]
			}
		}
	},

	componentWillReceiveProps: function(newProps) {
		if (newProps.request.caseNumber !== this.state.caseNumber)
			this.setState({caseNumber: newProps.request.caseNumber});
	},

	_setCaseNumber: function(caseNumber) {
		// turn on Activity Indicator
		this._setWorkflowStage("save", 1);

  	// 1b. Add status to existing tow request
  	RequestActions.setParam.triggerPromise(this.props.request.iid, ["caseNumber"], this.state.caseNumber)
			.then(() => {
				this._setWorkflowStage(this._currentWorkflow, 2);
			}).catch((err) => {
				this._setWorkflowStage(this._currentWorkflow, 3);
			});	  
	},

	_setWorkflowStage: function(workflow, level) {
    this._currentWorkflow = workflow;
    var workflowStages = _.map(this.state.workflowStages[workflow], (stage) => {
      stage.isActive = false;
      return stage;
    });

    workflowStages[level].isActive = true;
    var newState = {};
    this.state.workflowStages[workflow] = workflowStages;
    
    _.assign(newState, this.state.workflowStages);
    this.setState(newState);
  },

	render: function() {
		var imgHost = this.props.lookups.hosts["images"]
			, request = this.props.request
			, themeColors = this.props.themeColors
			, userTasks = this.props.currentSiteRight.task
			, CaseNumber, SaveStatus, SaveButtons
			, workflowStages = this.state.workflowStages[this._currentWorkflow];

		if (_.contains(userTasks, this.TaskIds.VIEW_STATUS)) {
			CaseNumber =
				<TextInput
					onChangeText={(value) => this.setState({caseNumber: value})}
			  	placeholder={"Enter case #"}
			    style={ [_styles.caseNumberValueBox, _styles.caseNumberInputBox, _styles.caseNumberText, {borderColor: themeColors["police"], color: themeColors["police"]}] }
			    value={this.state.caseNumber} />

			SaveButtons =
				<ActionButtons
					cancel={ () => this.setState({caseNumber: this.props.request.caseNumber}) }
					inputChanged={this.state.caseNumber !== this.props.request.caseNumber}
					saveData={() => this._setCaseNumber(this.state.caseNumber)}
					style={_styles.actionButtons} />

		  SaveStatus =
			  <Modal
			  	animation={false}
			  	visible={!workflowStages[0].isActive}>
	        <Pending
	          workflowStages={workflowStages}
	          setDone={() => this._setWorkflowStage(this._currentWorkflow, 0)}
	          style={_styles.submitting} />
	      </Modal>

		} else {
			CaseNumber =
				<View style={_styles.caseNumberValueBox}>
					<Text style={ [_styles.caseNumberText, {color: this.props.themeColor}] }>
						{this.state.caseNumber || "None"}
					</Text>
				</View>

			SaveButtons = null, SaveStatus = null;
		}

		if (this.props.sites.police) {
			var lastStatusEntry = _.last(request.statusEntries);
			var statusEntryStyle = StyleSheet.create({
				mainBox: {
					flex: 1,
					flexDirection: "row",
					paddingHorizontal: 2
				}, status: {
						fontSize: 20
					}
			});

			return (
				<View style={_styles.main}>
					{SaveStatus}
					<StatusEntry
						client={this.props.sites.client}
						entityType="site"
						showTimeAgo={true}
						showStatus={true}
						statusEntry={lastStatusEntry}
						styles={statusEntryStyle}
						vendor={this.props.sites.vendor} />
					<LineSeparator height={0.5} horizMargin={0} vertMargin={0} />
					<View style={_styles.section}>
						<Site
							info={this.props.sites.police}
							imgHost={imgHost}
							showImg={true}
							showPhoneBtn={true}
							style={_styles.site}
							themeColors={themeColors} />
					</View>
					<LineSeparator height={0.5} horizMargin={0} vertMargin={8} />
					<View style={_styles.section}>
				  	<View style={_styles.caseNumberTitleBox}>
				  		<Text style={ [_styles.caseNumberTitleText, {color: this.props.themeColor}] }>Case #</Text>
				  	</View>
				  	{CaseNumber}
				  </View>
			    {SaveButtons}
				</View>
			);
		}
		else
			return (
				<View style={_styles.main}>
					<Text
						numberOfLines={1}
						style={ [_styles.caseNumberTitleText, {color: this.props.themeColor}] }>No police dept assigned
					</Text>
				</View>
			);
	},
});

module.exports = PoliceScene;