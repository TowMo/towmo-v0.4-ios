'use strict';

// REACT PARTS
var NavBtn = require("react-native-button");
var Collapsible = require('react-native-collapsible/Collapsible');
var Display = require("react-native-device-display");
var Icon = require('react-native-vector-icons/Ionicons');
var NavBar = require("react-native-navbar");
var React = require("react-native");
// var Signature = require('react-native-signature-capture');

// COMPONENTS
var MapScene = require("./MapScene");
var NavBarTitle = require("../Comps/NavBarTitle");
var HistoryScene = require("./HistoryScene");
var InputHelperScene = require("./InputHelperScene");
var RequestInfoScene = require("./RequestInfoScene");
var VehicleInfoScene = require("./VehicleInfoScene");

// MIXINS
var RequestMixin = require("../Mixins/Request");
var SiteMixin = require("../Mixins/Site");
var ViewMixin = require("../Mixins/View");

// Utilities
var _ = require("lodash");

var {
	ActionSheetIOS,
	ListView,
	Modal,
	NativeModules: {
    RNHTMLtoPDF,
 //    RNPrint
  },
 	Navigator,
 	SegmentedControlIOS,
	StyleSheet,
	Text,
	TextInput,
	TouchableHighlight,
	View
} = React;

var BaseConfig = Navigator.SceneConfigs.FloatFromRight
var CustomLeftToRightGesture = _.assign({}, BaseConfig.gestures.pop, {
  snapVelocity: 2,
  edgeHitWidth: Display.width
});

var CustomSceneConfig = _.assign({}, BaseConfig, {
  springTension: 80,
  springFriction: 3,
  gestures: {
    pop: CustomLeftToRightGesture
  }
});

var styles = StyleSheet.create({
	mainBox: {
		flex: 1,
		opacity: 1.0
	},
	main: {
		flex: 1,
	},
	navBarTitle: {
		alignItems: "center",
		alignSelf: "center",
		flex: 8,
		justifyContent: "center",
		width: Display.width * 0.65,
	},
	navBtn: {
		color: "#FFFFFF",
		marginHorizontal: 12,
		flex: 2,
		fontSize: 32,
		textAlign: "center"
	},
	close: {
		position: "absolute",
		bottom: 0
	}
});

var RDMain = React.createClass({
	mixins: [RequestMixin, SiteMixin, ViewMixin],
	_loaded: false,
	_prevGeoPoint: null,
	_requestRef: null,
	_sites: {
		client: null,
		police: null,
		security: null,
		vendor: null
	},

	getInitialState: function() {
		var props = this.props.route.passProps;
		
		return {
			context: props.context,
			dims: props.dims,
			imageDims: null,
			nav: null,
			request: _.cloneDeep(props.request),
			scenes: null,
			sceneIndex: 0,
			signature: false
		};
	},

	componentWillMount: function() {
		var request = this.state.request;
		var sites = this.props.sites;

		// *** NEED TO REDO:  1) ready from request itself, otherwise 2) look @ client allies
		this._sites[this._orgTypeIds.CLIENT] = this._getSite(this._orgTypeIds.CLIENT);
		this._sites[this._orgTypeIds.VENDOR] = this._getSite(this._orgTypeIds.VENDOR);
		this._sites[this._orgTypeIds.SECURITY] = this._getSite(this._orgTypeIds.SECURITY);
		this._sites[this._orgTypeIds.POLICE] = this._getSite(this._orgTypeIds.POLICE);

		this._requestRef = this.props.db.child("requests").child(request.iid);
			
		// listener for any changes to current request
		this._requestRef.on("child_changed", (snap) => {
			this._updateRequest(snap);
		});

		this._requestRef.on("child_added", (snap) => {
			if (!this._loaded)
				return;

			this._updateRequest(snap);
		});
	},

	componentWillUnmount: function() {
		/*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
			Find out how to  prevent re-rendering when this component unmounts
		!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*/
		this._requestRef.off();
	},

	componentDidMount: function() {
		this._loaded = true;
	},

	shouldComponentUpdate: function(newProps, newState) {
  	var oldProps = this.props, oldState = this.state;
  	var stateChanged = false;

  	if ( !_.eq(newProps.requests, oldProps.requests) )
  		stateChanged = false;
  
  	if ( !_.eq(newState.request, oldState.request) )
  		stateChanged = true;
  	else
  		stateChanged = true;

  	return stateChanged;
  },

	_changeScene: function(e) {
		var newSceneIndex = e.nativeEvent.selectedSegmentIndex;
		if (this.state.sceneIndex != newSceneIndex)
			this.setState({
				sceneIndex: newSceneIndex,
			});
	},

	_getSite: function(orgTypeId) {
		var request = this.state.request
			, sites = this.props.sites;
	
		var siteId = _.has(request.sites, orgTypeId) ? request.sites[orgTypeId].siteId : null;
		
		if ( _.isEmpty(siteId) ) {
			var site = _.findWhere(this._sites[this._orgTypeIds.CLIENT].allies, {"isActive": true, "orgTypeId": orgTypeId});
			siteId = site ? site.id : null;
		}

		return _.isEmpty(siteId) ? null : sites[orgTypeId][siteId];
	},

	_goToScene: function(index) {
		this._momentary = true;
		this.setState({
			sceneIndex: index
		});
	},

	_moveInput: function(viewRef, targetRef, offset) {
    let scrollResponder = viewRef.getScrollResponder();
    let nodeHandle = React.findNodeHandle(targetRef);
    scrollResponder.scrollResponderScrollNativeHandleToKeyboard(nodeHandle, offset);
  },

	_openMap: function(themeColors) {
		if ((this._prevGeoPoint != null)
			&& ( _.matches(this._prevGeoPoint, this.state.request.geoPoint) ))
			this.props.navigator.jumpForward();
		else {
			var route = {
			  component: MapScene,
			  passProps: {
			  	currentSiteRight: this.props.currentSiteRight,
			  	dims: this.state.dims,
			  	request: this.state.request,
			  	sites: this._sites,
			  	themeColors: themeColors
			  },
			  transitionType: "FloatFromBottom"
			};
			
			this.props.navigator.push(route);
			this._prevGeoPoint = this.state.request.geoPoint;
		}
	},

	_openAssistedInput: function(imgTypeId, params, inputValue) {
		var route = {
		  component: InputHelperScene,
		  passProps: {
		  	sceneDims: this.state.dims,
		  	prevImg: _.findWhere(this.state.request.images, {"imgTypeId": imgTypeId}),
		  	imgTypeId: imgTypeId,
		  	inputValue: inputValue,
		  	params: params,
		  	request: this.state.request
		  },
		  transitionType: "FloatFromBottom"
		};

		this.props.navigator.push(route);
	},

	_setDims: function(e) {
		if (this.state.dims == null) {
			var layout = e.nativeEvent.layout; 
			
			this.setState({
				dims: {
					height: layout.height,
					width: layout.width,
				}
			});
		} else
			return;
  },

  _showDocumentOptions: function() {
  	var options = {
  		html: this.buildHTML(this.state.request),
  		fileName: "request-" +this.state.request.iid,
  		directory: "docs"
  	};

  	// console.log("does NOT work due to react-native-html-to-pdf causing error with Refresh module");

  	// this._showSignature(true);

  	var buttons = [
		  "Email",
		  "Print",
		  "Text",
		  'Cancel'
		];

  	ActionSheetIOS.showActionSheetWithOptions({
      options: buttons,
      cancelButtonIndex: 3
    }, (btnIndex) => {
      console.log("you selected: ", btnIndex);
    });

  	RNHTMLtoPDF.convert(options).then((filepath) => {
  		console.log(filepath);

  		// RNPrint.print(filepath).then((jobName) => {
  		// 	consoel.log("Printing ${jobName} complete!");
  		// });
  	});
  },

  _updateRequest: function(snap) {
		console.log("Tow Request has been updated");
		var request = this.state.request;
		request[snap.key()] = snap.val();

		this.setState({
			request: request
		});
  },

	_renderScene: function(route, nav) {	
		var navBar = null
			, props = this.props.route.passProps
			, request = this.state.request
			, Scene
			, themeColors = props.themeColors
			, users = this.props.users;

		if (route.navigationBar) {
		 	navBar = React.addons.cloneWithProps(route.navigationBar, {
		  	navigator: nav,
		  	route: route
		 	});
		}

		switch (this.state.sceneIndex) {
			case 1:
				Scene =
					<HistoryScene
	   				context="all"
	   				currentSiteRight={this.props.currentSiteRight}
	   				currentUser={this.props.currentUser}
	   				ds={ new ListView.DataSource({rowHasChanged: (r1, r2) => r1.guid !== r2.guid}) }
	   				lookups={this.props.lookups}
	   				nav={nav}
	   				request={request}
	   				sites={this._sites}
	   				statusLookups={this.props.lookups.statuses}
	   				themeColors={themeColors}
	   				users={users} />

			break;

			case 2:
				Scene =
					<VehicleInfoScene
						currentSiteRight={this.props.currentSiteRight}
						currentUser={this.props.currentUser}
						ds={ new ListView.DataSource({rowHasChanged: (r1, r2) => r1.guid !== r2.guid}) }
						lookups={this.props.lookups}
						nav={nav}
						openAssistedInput={this._openAssistedInput}
						request={request}
						sites={this._sites}
						themeColors={themeColors}
						users={users} />
			break;

			default:
				Scene =
					<RequestInfoScene
						currentSiteRight={_.cloneDeep(this.props.currentSiteRight)}
						currentUser={this.props.currentUser}
						dims={props.dims}
						lookups={this.props.lookups}
						moveInput={this._moveInput}
						nav={nav}
						openMap={this._openMap}
						request={request}
						sites={this._sites}
						themeColors={themeColors}
						users={users} />
			break;
		}

		return (
			<View style={styles.mainBox}>
				{navBar}
		   	<View
		   		style={styles.main}
		   		onLayout={this._setDims}>
		   		{Scene}
		   	</View>
			</View>
		);
	},

	_configureScene: function() {
		return CustomSceneConfig;
	},

	_saveSignature: function(result) {
		console.log(result);
	},

	_showSignature: function(state) {
		this.setState({
			signature: state
		});
	},

	render: function() {
		var props = this.props.route.passProps;
		var themeColor = props.themeColors[this.props.currentSiteRight.orgTypeId];
		var backBtn =
			<NavBtn onPress={this.props.navigator.pop}>
				<Icon name={"ios-arrow-back"} style={styles.navBtn} />
			</NavBtn>;

		var printBtn =
			<NavBtn onPress={this._showDocumentOptions}>
				<Icon name={"document-text"} style={styles.navBtn} />
			</NavBtn>;

		var navBarTitle =
			<SegmentedControlIOS
				enabled={true}
				onChange={this._changeScene}
				selectedIndex={this.state.sceneIndex}
				style={styles.navBarTitle}
				tintColor="#FFFFFF"
				values={["Home", "History", "Vehicle"]} />

		var navBar =
			<NavBar
				backgroundColor={themeColor}
				buttonsColor="#FFFFFF"
				customNext={printBtn}
				customPrev={backBtn}
				nextTitle="Print"
				prevTitle="Back"
				customTitle={navBarTitle} />

		if (this.state.signature)
			return (
				<Modal
					animation={false}
					visible={this.state.signature}>
					<Signature onSaveEvent={this._saveSignature} />
					<TouchableHighlight
						underlayColor="#A4A4A4"
						onPress={() => this._showSignature(false)}
						style={styles.close}>
						<View>
							<Text>Cancel</Text>
						</View>
					</TouchableHighlight>
				</Modal>
			);
		else {
			return (
				<Navigator
					configureScene={this._configureScene}
					renderScene={this._renderScene}
					initialRoute={{
					  navigationBar: navBar,
					  themeColors: props.themeColors
					}} />
			);
		}
	}
});

module.exports = RDMain;