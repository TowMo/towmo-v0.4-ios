'use strict';

// REACT PARTS
var Display = require('react-native-device-display');
var Icon = require('react-native-vector-icons/Ionicons');
var React = require("react-native");
var Reflux = require("reflux");
var Swiper = require("react-native-swiper");
var TimerMixin = require('react-timer-mixin');

// COMPONENTS
var ActionButtons = require("../Comps/ActionButtons");
var LineSeparator = require("../Comps/LineSeparator");
var NextStatuses = require("./NextStatuses");
var Pending = require("../Comps/Pending");
var RequestImages = require("../Comps/RequestImages");
var Site = require("../Comps/Site");
var StatusEntry = require("../Comps/StatusEntry");
var TodoSet = require("../_NewRequest/TodoSet");
var WhenMgr = require("../Comps/WhenMgr");

// ACTIONS && STORES
var MapActions = require("../Actions/MapActions");
var RequestActions = require("../Actions/RequestActions");
var UserActions = require("../Actions/UserActions");

// MIXINS
var LocationMixin = require("../Mixins/Location");
var RequestMixin = require("../Mixins/Request");
var SiteMixin = require("../Mixins/Site");
var ViewMixin = require("../Mixins/View");

// Utilities
var _ = require("lodash");
var Moment = require("moment");

var {
	AlertIOS,
	MapView,
	Modal,
	PropTypes,
	ScrollView,
	StyleSheet,
	SwitchIOS,
	Text,
	TextInput,
	TouchableHighlight,
	View,
} = React;

var styles = StyleSheet.create({
	container: {
		flex: 1,
	},
	main: {
		flex: 1,
	},
});

var RequestInfoScene = React.createClass({
	mixins: [Reflux.ListenerMixin, TimerMixin, RequestMixin
				, LocationMixin, SiteMixin, ViewMixin],
	propTypes: {
		currentUser: PropTypes.object,
		currentSiteRight: PropTypes.object,
		dims: PropTypes.object,
		lookups: PropTypes.object,
		moveInput: PropTypes.func,
		nav: PropTypes.object,
		openMap: PropTypes.func,
		request: PropTypes.object,
		sites: PropTypes.object,
		themeColors: PropTypes.array,
		users: PropTypes.array
	},
	_setStatusNote: "",
	_styles: StyleSheet.create({
		actionButtons: {
			alignItems: "center",
			// bottom: ViewMixin.Dimensions.ACTION_BTN_HEIGHT,
			bottom: 0,
			flexDirection: "row",
			position: "relative",
			width: Display.width
		},
		main: {
			flexDirection: "column",
		},
	  mapSection: {
	  	borderWidth: 1.25,
	  	flex: 1,
	  },
	  primaryInfo: {
			flex: 1,
			flexDirection: "column"
		},
		settingStatus: {
			height: Display.height,
      justifyContent: "center",
      width: Display.width
		},
		section: {
			flex: 1,
			flexDirection: "column",
			marginHorizontal: 2,
			marginVertical: 4
		},
			sectionTitle: {
				fontFamily: "System",
				fontSize: 32,
				fontWeight: "200"
			}
	}),
	_statusToApprove: null,
	_timer: null,
	_workflowMessages: {
		"save": ["Waiting to Save", "Trying to Save...", "Site Information saved!", "Error: Couldn\'t save"]
	},

	getInitialState: function() {
		return {
			mapParams: null,
			request: null,
			requirementsMet: false,
			showMap: false,
			sitesRefCopy: null,
			workflowStages: [
      	{
          isActive: true,
          end: false,
          success: true
        }, {
          isActive: false,
          end: false,
          success: true
        }, {
          isActive: false,
          end: true,
          success: true
        }, {
          isActive: false,
          end: true,
          success: false
        }
      ]
		};
	},

	componentWillMount: function() {
		// this._refreshStatusToApprove(this.props, this._setPreApprovalVisibility);
		this._refreshRequest(this.props);
		this._resetSitesRefCopy(this.props);
		this._refreshMap(this.props);
		this.setState(this.state);
	},

	componentDidMount: function() {
		this._timer = this.setTimeout(() => {
      this.setState({
      	showMap: true,
      });
    }, 500);
	},

	componentWillUpdate: function(newProps, newState) {
		var stateChanged = false;
		var oldProps = this.props, oldState = this.state;

		if ( !_.isEqual(newProps.request.geoPoint, oldState.request.geoPoint) )
			stateChanged = this._refreshMap(newProps);

		if ( !_.isEqual(newProps.currentUser, oldProps.currentUser) || !_.isEqual(newProps.currentSiteRight, oldProps.currentSiteRight) )
			stateChanged = this._refreshRequest(newProps);

		if ( !_.isEqual(newProps.request, oldState.request) )
			stateChanged = this._refreshRequest(newProps);

		if (stateChanged)
			this.setState(oldState);
	},

	componentWillUnmount: function() {
		this.clearTimeout(this._timer);
	},

	// _moveInput: function(targetRef, offset) {
 //    let scrollResponder = this.refs.scrollView.getScrollResponder();
 //    let nodeHandle = React.findNodeHandle(targetRef);
 //    scrollResponder.scrollResponderScrollNativeHandleToKeyboard(nodeHandle, offset);
 //  },

  _moveInput: function(targetRef, offset) {
		this.props.moveInput(this.refs.scrollView, targetRef, offset);
	},

	_refreshMap: function(newProps) {
		var geoPoints = new Array(newProps.request.geoPoint);
		var mapParams = this.getMapParams(geoPoints, newProps.dims);
		
		_.assign(mapParams, {
			"annotations": [{
				latitude: newProps.request.geoPoint.lat,
				longitude: newProps.request.geoPoint.long,
				hasLeftCallout: false,
			}]
		});
	
		/********************************************************************
			!!!!!!!! NEED TO SOURCE ChosenState FROM USER SETTINGS !!!!!!!!!
		********************************************************************/
		this.state.mapParams = mapParams;
		this.state.requirementsMet = true;

		return true;
	},

	_refreshRequest: function(props) {
		this.state.request = _.cloneDeep(props.request);
		return true;
	},

	_resetSitesRefCopy: function(props, setState) {
		this.state.sitesRefCopy = props.request.sites;
		
		if (setState)
			this.setState(this.state);
		else
			return true;
	},

	_saveSites: function() {
		this._setWorkflowStage("save", 1);
		var qSites = [];

		_.each(this.state.sitesRefCopy, (siteRefCopy, key) => {
			if ( !_.isMatch(this.state.request.sites[key], siteRefCopy) )
				qSites.push(RequestActions.setParam.triggerPromise(this.props.request, ["sites", key], siteRefCopy));
		});

		new Promise.all(qSites).then((results) => {
			console.log("New data saved to Firebase...");
			this._setWorkflowStage("save", 2);
		}).catch((err) => {
			console.log("Problem saving to Firebase: ", err);
			this._setWorkflowStage("save", 3);
		})
	},

	_setTodoSet: function(path, newSet) {
		path.push("todoItems");
		_.set(this.state.sitesRefCopy, path, newSet);
		this.setState(this.state);
	},

	_setWorkflowStage: function(workflow, level) {
    this._currentWorkflow = workflow;
    var workflowStages = _.map(this.state.workflowStages, (stage) => {
      stage.isActive = false;
      return stage;
    });

    workflowStages[level].isActive = true;
    this.setState({
    	workflowStages: workflowStages
    });
  },

	render: function() {
		var props = this.props, state = this.state;
		var currentSiteRight = props.currentSiteRight
			, currentUser = props.currentUser
			, dims = props.dims
			, lookups = props.lookups
			, mapParams = state.mapParams
			, request = state.request
			, sites = _.omit(props.sites, _.isEmpty)
			, themeColors = props.themeColors
	  	, imgUris = _.pluck(request.images, "uri")
	  	, lastStatusEntry = _.last(request.statusEntries)
	  	, MapSection = state.showMap && state.mapParams
		  	? <MapView
		  			annotations={mapParams.annotations}
		  			region={mapParams.region}
		  			rotateEnabled={false}
		  			scrollEnabled={false}
		  			style={{height: dims.width / this.AspectRatios["21x9"], width: dims.width}}
						zoomEnabled={false}>
					</MapView>
				: null;
		
		var statusEntryStyle = StyleSheet.create({
			mainBox: {
				alignSelf: "center",
				flex: 1,
				flexDirection: "row",
				paddingHorizontal: 2
			},
			// status: {
			// 		fontSize: 20
			// 	}
		});

		var viewHeight = Display.height
								- (this.Dimensions.STATUS_BAR_HEIGHT * 2)
								- (this.Dimensions.TAB_BAR_HEIGHT * 2)
								- (this._showPreApproval ? approvalStyle.height : 0);

		return (
			<View style={ [this._styles.main] }>
	   		<StatusEntry
					currentUser={props.currentUser}
        	currentSiteRight={currentSiteRight}
					lookups={lookups}
					request={request}
					show={{status: true, update: true}}
					sites={sites}
					statusEntry={lastStatusEntry}
					styles={statusEntryStyle}
					themeColors={props.themeColors}
					users={props.users} />
				<LineSeparator height={0.5} horizMargin={0} vertMargin={0} />
				
		   	<ScrollView
		   		contentInset={{top: -(this.Dimensions.STATUS_BAR_HEIGHT * 3 / 4)}}
		   		keyboardShouldPersistTaps={false}
		   		ref="scrollView"
	        scrollEventThrottle={200}
	        style={{height: viewHeight}}>
					<View style={this._styles.primaryInfo}>
						<RequestImages
			   			aspectRatio={16/10}
			   			imgHost={lookups.hosts["images"]}
							uris={imgUris} />
						<LineSeparator vertMargin={4} height={0} />
						<TouchableHighlight
							underlayColor="#A4A4A4"
							onPress={() => props.openMap(themeColors)}>
							<View style={{borderColor: themeColors[currentSiteRight.orgTypeId]}}>
					  		{MapSection}
							</View>
						</TouchableHighlight>
						<LineSeparator height={0} horizMargin={0} vertMargin={4} />
						{_.map(sites, (site) => {
							var siteRef = state.sitesRefCopy[site.orgTypeId];

							return !_.isEmpty(siteRef) ?
								<SiteSection
									key={site.orgTypeId}
									currentSiteRight={currentSiteRight}
									lookups={lookups}
									moveInput={this._moveInput}
									setTodoSet={this._setTodoSet}
									site={site}
									themeColors={themeColors}
									todoEntrySet={siteRef.todoItems} /> :
								null;
						})}
					</View>
				</ScrollView>
				<ActionButtons
					cancel={() => this._resetSitesRefCopy(this.props, true)}
					inputChanged={!_.isMatch(request.sites, state.sitesRefCopy)}
					saveData={this._saveSites}
					style={this._styles.actionButtons} />
			</View>
		);
	}
});


/*********************************************************************************************************
																				S I T E     S E C T I O N
*********************************************************************************************************/
var SiteSection = React.createClass({
	mixins: [RequestMixin, ViewMixin],
	propTypes: {
		currentSiteRight: PropTypes.object,
		lookups: PropTypes.object,
		moveInput: PropTypes.func,
		setTodoSet: PropTypes.func,
		site: PropTypes.object,
		themeColors: PropTypes.array,
		todoEntrySet: PropTypes.object
	},

	_styles: StyleSheet.create({
		main: {
			backgroundColor: ViewMixin.Colors.night.block,
			borderWidth: 1,
			flex: 1,
			flexDirection: "column",
			marginBottom: 6,
			padding: 4
		}, site: {
				flex: 1,
				flexDirection: "row",
				paddingVertical: 2
			},
			todoItemHorz: {
				flex: 1,
				flexDirection: "row",
				padding: 6
			}, todoTitleHorz: {
					flex: 5
				},
				todoValueHorz: {
					flex: 1,
					justifyContent: "center"
				},
				
			todoItemVert: {
				flex: 1,
				flexDirection: "column",
				paddingVertical: 6
			}, 
				todoTitleVert: {
					borderColor: "#A4A4A4",
					flex: 1,
					paddingBottom: 2,
					paddingHorizontal: 6
				},
				todoValueVert: {
					flex: 1,
					paddingHorizontal: 6,
					paddingVertical: 2
				},
					
			ttText: {
				color: "#E8E8E8",
				fontFamily: "System",
				fontSize: 18,
				fontWeight: "200"
			},
			tvText: {
				// fontSize: 18,
				fontFamily: "System",
				fontWeight: "200",
				letterSpacing: 2
			}
	}),

	getInitialState: function() {
		return {
			showWhenMgr: false
		};
	},

	_moveInput: function(target, offset) {
		this.props.moveInput(this.refs[target], offset);
	},

	// Scroll a component into view. Just pass the component ref string.
	_toggleShowWhenMgr: function(state, setState) {
		this.state.showWhenMgr = state;

		if (setState)
			this.setState(this.state);
		else
			return true;
	},

	render: function() {
		var props = this.props;
		var currentSiteRight = props.currentSiteRight
			, lookups = props.lookups
			, site = props.site
			, themeColors = props.themeColors
			, todoSet = lookups.orgTypes[site.orgTypeId].todos
			, todoEntrySet = _.mapKeys(props.todoEntrySet, (todoItem) => {
				return todoItem.todoId;
			}); /* For some reason, todoSets/Items of each orgType is being modified from
						key-value pairs to having index-based values */
		var color = this.isDone(_.pluck(todoEntrySet, "value")) ? lookups.orgTypes[site.orgTypeId].color : this.Colors.night.border
			, TodoComp = _.isEmpty(todoSet) ? null :
					<TodoSet
	          editable={_.contains(todoSet.accessRights.write, currentSiteRight.orgTypeId)}
	          key={site.orgTypeId}
	          moveInput={props.moveInput}
	          orgType={lookups.orgTypes[site.orgTypeId]}
	          setTodo={(newSet, path) => props.setTodoSet(path, newSet)}
	          siteRight={currentSiteRight}
	          todoEntrySet={todoEntrySet}
	          todoSet={todoSet.options} />

		return (
			<View style={ [this._styles.main, {borderColor: color}] }>
				<Site
					info={site}
					imgHost={lookups.hosts["images"]}
					showImg={true}
					showPhoneBtn={true}
					style={this._styles.site}
					themeColors={themeColors} />
				{TodoComp}
			</View>
		);
	}
});

module.exports = RequestInfoScene;