'use strict';

// REACT PARTS
var Display = require('react-native-device-display');
var Icon = require('react-native-vector-icons/Ionicons');
var React = require("react-native");
var Reflux = require("reflux");

// COMPONENTS
var ActionButtons = require("../Comps/ActionButtons");
var LineSeparator = require("../Comps/LineSeparator");
var MenuSelect = require("../Comps/MenuSelect");
var Pending = require("../Comps/Pending");

// MIXINS
var RequestMixin = require("../Mixins/Request");
var ViewMixin = require("../Mixins/View");

// ACTIONS && STORES
var RequestActions = require("../Actions/RequestActions");

// Utilities
var moment = require("moment");
var _ = require("lodash");

var {
	ListView,
	Modal,
	PropTypes,
	StyleSheet,
	Text,
	TextInput,
	TouchableHighlight,
	View
} = React;

var VehicleEditForm = React.createClass({
	propTypes: {
		currentSiteRight: PropTypes.object,
		ds: PropTypes.object,
		lookups: PropTypes.object,
		// moveInput: PropTypes.func,
		nav: PropTypes.object,
		openAssistedInput: PropTypes.func,
		request: PropTypes.object,
		themeColors: PropTypes.array
  },
  mixins: [RequestMixin, ViewMixin],
	_currentWorkflow: "save",
	_prevYears: [], /* FUTURE PLANS:  allow user to adjust settings for how many years back */
	_vehicleFields: {},
	_workflowMessages: {
		"save": ["Waiting to Save", "Trying to Save...", "New status saved!", "Error: Couldn\'t save"],
		"search": ["idle", "Searching Database...", "Great! Vehicle found!!", "Shit! Nothing found!"]
	},

	_styles: StyleSheet.create({
	  main: {
	  	flex: 1,
	  	flexDirection: "column"
	  },

	  menuBox: {
			backgroundColor: "#000000",
			height: Display.height,
			flex: 1,
			flexDirection: "column",
			opacity: 0.8,
			padding: 4,
			position: "absolute",
			width: Display.width
		},

	  actionButtons: {
			bottom: ViewMixin.Dimensions.TAB_BAR_HEIGHT,
			flexDirection: "row",
			alignItems: "center",
			width: Display.width - 8
		},

		param: {
			flex: 1,
			flexDirection: "row",
			marginVertical: 5,
			marginHorizontal: 2
		}, input: {
				borderWidth: 0.5,
				padding: 7
			}, inputText: {
					fontSize: 19,
					fontFamily: "System",
					// fontWeight: "200",
					height: 30,
					letterSpacing: 1
				},
			iconView: {
				alignItems: "center",
				flex: 1,
				justifyContent: "center",
				paddingVertical: 7
			}, iconText: {
					color: "#A4A4A4",
					fontSize: 26,
					textAlign: "center"
				},
		submitting: {
		  height: Display.height,
		  justifyContent: "center",
		  width: Display.width,
		}
	}),

	getInitialState: function() {
		return {
			ds: new ListView.DataSource({rowHasChanged: (r1, r2) => r1.guid !== r2.guid}),
			options: null,
			showMenu: false,
			vehicleCopy: _.cloneDeep(this.props.request.vehicle),
			vehicleParam: null,
			workflowStages: [
        {
          isActive: true,
          end: false,
          success: true
        }, {
          isActive: false,
          end: false,
          success: true
        }, {
          isActive: false,
          end: true,
          success: true
        }, {
          isActive: false,
          end: true,
          success: false
        }
      ]
		};
	},

	componentWillMount: function() {
		var currentYear = moment().year();
		
		_.times(50, (n) => {
			var year = (currentYear - n).toString();
			this._prevYears.push(year);
		});

		_.each(this.props.lookups.vehicle.options, (vehicleParam, key) => {
			this._vehicleFields[key] = this._buildField(key, this.props, this.state);
		});
	},

	componentWillUnmount: function() {
		if ( !_.eq(this.state.vehicleCopy, this.props.request.vehicle) )
			console.log("vehicle && vehicleCopy are not the same");
	},

	componentWillUpdate: function(newProps, newState) {
		var oldList = this.state.vehicleCopy;
		
		_.each(newState.vehicleCopy, (param, key) => {
			if ( !_.eq(param, oldList[key]) )
				this._vehicleFields[key] = this._buildField(key, newProps, newState);
		});
	},

	_buildField: function(key, props, state) {
		var vehicleCopy = state.vehicleCopy
			, vehicle = props.lookups.vehicle.options
			, param = vehicle[key]
			, paramRef = vehicleCopy[key]
  		, inputFlex = 8 - (_.has(param, "parts") ? param.parts.length : 0)
  		, lookups = props.lookups
  		, request = props.request
			, baseValue = " --- " +param.title +" --- "
			, styles = this._styles
			, themeColor = props.themeColors[props.currentSiteRight.orgTypeId]
		
		var done = _.has(param.lengths) ? (paramRef.value.length >= param.lengths.required) : !_.isEmpty(paramRef.value);
		var Content, LeftIcon, RightIcon, options, value, inputStyle;
		
		inputStyle = done ? {borderColor: themeColor, borderWidth: 1} : this.Styles._viewStyle.off;

		if ( _.has(param, "age") )
			options = this._prevYears;
		else if ( _.has(param, "ref") )
			options = lookups[param.ref];
		else
			options = param.options;

		if (_.isEmpty(paramRef.value) || !options)
			value = paramRef.value;
		else
			value = options[paramRef.value] ? options[paramRef.value].name : paramRef.value;

		value = !_.isEmpty(value) ? value.toUpperCase() : value;

		switch(param.inputWay) {
			case "input":
				Content =
					<View style={[styles.input, {flex: inputFlex}, inputStyle]}>
						<TextInput
							placeholder={baseValue}
							placeholderTextColor="#A4A4A4"
					  	onChangeText={(newValue) => this._updateVehicleCopy(newValue, [param.iid, "value"])}
							style={[styles.inputText, done ? {color: themeColor} : this.Styles._textStyle.off]}
							value={value} />
					</View>
				break;

			case "link":
				Content =
					<TouchableHighlight
						onPress={() => props.openAssistedInput(key, [key], paramRef.value)}
						style={[styles.input, {flex: inputFlex}, inputStyle]}>
				  	<Text style={[styles.inputText, done ? {color: themeColor} : this.Styles._textStyle.off]}>{value || baseValue}</Text>
			  	</TouchableHighlight>
				break;

			case "select":
				Content = 
	      	<TouchableHighlight
	      		onPress={() => this._prepMenu(key, options)}
	      		style={[styles.input, {flex: inputFlex}, inputStyle]}>
					  <Text style={[styles.inputText, done ? {color: themeColor} : this.Styles._textStyle.off]}>{value || baseValue}</Text>
				  </TouchableHighlight>
				break;

			default:
				Content = <View></View>;
				break;
		}

		return (
			<View key={key} style={styles.param}>
				{this._renderIcon("left", param, paramRef.value)}
				{Content}
				{this._renderIcon("right", param, paramRef.value)}
			</View>
		);
	},

	// _moveInput: function(target, offset) {
	// 	this.props.moveInput(this.refs.listView, this.refs[target], offset);
	// },

	_prepMenu: function(vehicleParam, options) {
		this.setState({
			options: _.toArray(options),
			vehicleParam: vehicleParam
		});

		this._toggleMenu(true);
	},

	_resetForm: function() {
		this.setState({
			vehicleCopy: _.cloneDeep(this.props.request.vehicle),
		});
	},

	_saveFormData: function() {
		// compare initial vehicle data vs. vehicleCopy

		// have RequestActions update vehicle Data in Firebase
		this._setWorkflowStage("save", 1);

		RequestActions.setParam.triggerPromise(this.props.request, ["vehicle"], this.state.vehicleCopy)
			.then(() => {
				console.log("New data saved to Firebase...");
				this._setWorkflowStage("save", 2);
			}).catch((err) => {
				console.log("Problem saving to Firebase: ", err);
			});
	},

	_searchVehicleInfo: function(vin) {
		this._setWorkflowStage("search", 1);
		var lookups = this.props.lookups;

		RequestActions.pullVehicleData(vin).then((vehicleData) => {
			this._updateVehicleCopy(vehicleData);
			this._setWorkflowStage("search", 2);
		}).catch((err) => {
			this._setWorkflowStage("search", 3);
		});
	},

	_setWorkflowStage: function(workflow, level) {
    this._currentWorkflow = workflow;
    var workflowStages = _.map(this.state.workflowStages, (stage) => {
      stage.isActive = false;
      return stage;
    });

    workflowStages[level].isActive = true;
    this.setState({
    	workflowStages: workflowStages
    });
  },

  _toggleMenu: function(state) {
		this.setState({
			showMenu: state
		});
	},

  _updateVehicleCopy: function(value, path) {
		if (this.state.showMenu === true)
			this._toggleMenu(false);

		this.setState((oldState, oldProps) => {
			var oldVehicleCopy = _.cloneDeep(oldState.vehicleCopy);
			
			if (_.isEmpty(path))
				_.each(value, (paramValue, param) => {
					oldVehicleCopy[param].value = paramValue;
				});
			else
				_.set(oldVehicleCopy, path, value);
			
			return { vehicleCopy: oldVehicleCopy };
		});
	},

  _renderIcon: function(side, param, paramRefVal) {
  	// determine whether other validation is needed for icon color
  	var iconMeta = _.findWhere(param.parts, {"side": side});
  	
  	if (!iconMeta)
  		return null;
  	else {
  		var obj
	  		, props = this.props
	  		, ref = iconMeta.ref
	  		, themeColor = props.themeColors[props.currentSiteRight.orgTypeId];

	  	if (!_.isEmpty(ref)) {
	  		var condition = {};
				condition[ref.key] = param.iid;
				obj = _.findWhere(props.request[ref.param], condition);
	  	}

	  	var IconView = <Icon name={iconMeta.icon} style={[this._styles.iconText, _.isEmpty(obj) ? this.Styles._textStyle["n/a"] : {color: themeColor}]} />

			return iconMeta.action
				? (<TouchableHighlight
						onPress={() => this._searchVehicleInfo(paramRefVal)}
						style={this._styles.iconView}>
						{IconView}
					</TouchableHighlight>)
				: (<View style={this._styles.iconView}>{IconView}</View>);
  	}
  },

  _renderField: function(param, section, rowId) {		
		return (this._vehicleFields[param.iid]);
  },

	render: function() {
		var props = this.props, state = this.state
		var lookups = props.lookups
			, request = props.request
			, styles = this._styles
			, vehicleCopy = state.vehicleCopy;
		
		var workflowStages = state.workflowStages;
		var vehicleParams = _.sortByOrder(lookups.vehicle.options, ["rank"], ["asc"]);

		return (
			<View style={styles.main}>
				<Modal
					animation={false}
					visible={!workflowStages[0].isActive}>
	        <Pending
	        	workflowMessages={this._workflowMessages[this._currentWorkflow]}
	          workflowStages={workflowStages}
	          setDone={() => this._setWorkflowStage(this._currentWorkflow, 0)}
	          style={styles.submitting} />
	      </Modal>
	      <Modal
					animation={false}
					visible={state.showMenu}>
	        <MenuSelect
	        	choice={_.isEmpty(state.vehicleParam) ? null : state.vehicleCopy[state.vehicleParam].value}
	        	ds={state.ds}
	        	options={state.options}
	        	style={this._styles.menuBox}
	        	setValue={(newValue) => this._updateVehicleCopy(newValue, [state.vehicleParam, "value"])} />
	      </Modal>
	      <ListView
					contentInset={{top: -this.Dimensions.STATUS_BAR_HEIGHT}}
					dataSource={props.ds.cloneWithRows(vehicleParams)}
					initialListSize={vehicleParams.length}
					keyboardShouldPersistTaps={false}
					ref="listView"
					renderRow={this._renderField}
					scrollEventThrottle={200} />
				<ActionButtons
					cancel={this._resetForm}
					inputChanged={ !_.eq(state.vehicleCopy, this.props.request.vehicle) }
					saveData={this._saveFormData}
					style={styles.actionButtons} />
			</View>
		);
	}
});

module.exports = VehicleEditForm;