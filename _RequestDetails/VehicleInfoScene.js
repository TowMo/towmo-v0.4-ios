'use strict';

// REACT PARTS
var Display = require('react-native-device-display');
var Icon = require('react-native-vector-icons/Ionicons');
var NavBar = require("react-native-navbar");
var React = require("react-native");
var Reflux = require("reflux");

// COMPONENTS
var ActionButtons = require("../Comps/ActionButtons");
var LineSeparator = require("../Comps/LineSeparator");
var StatusEntry = require("../Comps/StatusEntry");

// MIXINS
var RequestMixin = require("../Mixins/Request");
var ViewMixin = require("../Mixins/View");

// COMPONENTS
var VehicleEditForm = require("./VehicleEditForm");

// ACTIONS && STORES
// var HostStore = require("../Stores/HostStore");
var RequestActions = require("../Actions/RequestActions");

// Utilities
var _ = require("lodash");

var {
	ListView,
	Modal,
	PropTypes,
	StyleSheet,
	Text,
	TouchableHighlight,
	View
} = React;

var VehicleInfoScene = React.createClass({
	propTypes: {
		currentSiteRight: PropTypes.object,
		currentUser: PropTypes.object,
		ds: PropTypes.object,
		lookups: PropTypes.object,
		moveInput: PropTypes.func,
		nav: PropTypes.object,
		openAssistedInput: PropTypes.func,
		request: PropTypes.object,
		sites: PropTypes.object,
		themeColors: PropTypes.array,
		users: PropTypes.array
  },
  mixins: [RequestMixin, ViewMixin],
  _styles: StyleSheet.create({
	  main: {
	  	flex: 1,
	  	flexDirection: "column",
	  	height: Display.height - ViewMixin.Dimensions.STATUS_BAR_HEIGHT - ViewMixin.Dimensions.NAV_BAR_HEIGHT - ViewMixin.Dimensions.TAB_BAR_HEIGHT
	  }
	}),

	render: function() {
		var props = this.props, state = this.state;
		var request = props.request;
		var lastStatusEntry = _.last(request.statusEntries);
		var statusEntryStyle = StyleSheet.create({
			mainBox: {
				alignSelf: "center",
				flex: 1,
				flexDirection: "row",
				paddingHorizontal: 2
			},
			// status: {
			// 		fontSize: 20
			// 	}
		});
		var vehicleLookup = props.lookups.vehicle;
		var InfoForm = _.contains(vehicleLookup.accessRights.write, props.currentSiteRight.orgTypeId)
			? <VehicleEditForm {...props} />
			: <VehicleReadForm {...props} />;
		
		return (
			<View style={this._styles.main}>
				<StatusEntry
					currentUser={props.currentUser}
        	currentSiteRight={props.currentSiteRight}
					lookups={props.lookups}
					request={request}
					show={{status: true, update: true}}
					sites={props.sites}
					statusEntry={lastStatusEntry}
					styles={statusEntryStyle}
					themeColors={props.themeColors}
					users={props.users} />
				<LineSeparator height={0.5} horizMargin={0} vertMargin={0} />
				{InfoForm}
			</View>
		);
	}
});


var VehicleReadForm = React.createClass({
	propTypes: {
		currentSiteRight: PropTypes.object,
		ds: PropTypes.object,
		lookups: PropTypes.object,
		nav: PropTypes.object,
		openAssistedInput: PropTypes.func,
		request: PropTypes.object,
		themeColors: PropTypes.array
  },
  mixins: [RequestMixin, ViewMixin],
	_styles: StyleSheet.create({
	  mainBox: {
	  	flex: 1,
	  	flexDirection: "column",
	  	marginVertical: 4,
	  },
		field: {
			flexDirection: "row",
			alignItems: "center",
			paddingVertical: 12
		}, fieldPart: {
				paddingHorizontal: 4
			}, labelText: {
					fontSize: 17,
					fontFamily: "System",
					fontWeight: "200",
					textAlign: "right"
				},
				valueText: {
					fontSize: 18,
					letterSpacing: 1,
					textAlign: "left"
				},

		submitting: {
		  height: Display.height,
		  justifyContent: "center",
		  width: Display.width,
		},
	}),

	_renderParam: function(param, sectionId, rowId) {
		var props = this.props
			, requestVehicle = props.request.vehicle
			, vehicle = props.lookups.vehicle.options
			, paramRef = requestVehicle[param.iid]
			, themeColor = props.themeColors[props.currentSiteRight.orgTypeId]
			, done = _.has(param.lengths) ? (paramRef.value.length >= param.lengths.required) : !_.isEmpty(paramRef.value);

		return (
			<View key={param.iid} style={this._styles.field}>
      	<View style={ [this._styles.fieldPart, {flex: 3}] }>
	      	<Text style={[this._styles.labelText, done ? {color: themeColor} : this.Styles._textStyle.off] }>{param.title}</Text>
				</View>			
				<View style={ [this._styles.fieldPart, {flex: 7}] }>
					<Text style={ [this._styles.valueText, done ? {color: themeColor} : this.Styles._textStyle.off] }>{paramRef.value.toUpperCase() || "----------"}</Text>
				</View>
		  </View>
		);
	},

	render: function() {
		var props = this.props;
		var vehicleParams = _.sortByOrder(props.lookups.vehicle.options, ["rank"], ["asc"])

		return (
			<ListView
				contentInset={{top: -(this.Dimensions.STATUS_BAR_HEIGHT / 4)}}
				dataSource={props.ds.cloneWithRows(vehicleParams)}
				initialListSize={vehicleParams.length}
				renderRow={this._renderParam}
				scrollEventThrottle={200}
				style={this._styles.mainBox} />
		);
	}
});

module.exports = VehicleInfoScene;