'use strict';

// REACT PARTS
var Display = require("react-native-device-display");
var Icon = require('react-native-vector-icons/Ionicons');
var NavBar = require("react-native-navbar");
var NavBtn = require("react-native-button");
var React = require("react-native");
var Reflux = require("reflux");

// CONTEXT
var RequestDetails = require("../_RequestDetails/RDMain");

// COMPONENTS
var LineSeparator = require("../Comps/LineSeparator");
var MenuSelect = require("../Comps/MenuSelect");
var NavItem = require("../Comps/NavItem");

// MIXINS
var SiteMixin = require("../Mixins/Site");
var RequestMixin = require("../Mixins/Request");
var ViewMixin = require("../Mixins/View");

// SCENES
var RequestListScene = require("./RequestListScene");
var RequestMapScene = require("./RequestMapScene");

// ACTIONS && STORES
var ProfileActions = require("../Actions/ProfileActions");
var RequestActions = require("../Actions/RequestActions");
var RequestStore = require("../Stores/RequestStore");
var SiteActions = require("../Actions/SiteActions");
var UserActions = require("../Actions/UserActions");
var UserStore = require("../Stores/UserStore");

// UTILITIES
var Moment = require("moment");
var _ = require("lodash");

var {
	ActivityIndicatorIOS,
	AlertIOS,
	Image,
	ListView,
	Modal,
 	Navigator,
 	PropTypes,
	StyleSheet,
	TabBarIOS,
	TouchableHighlight,
	Text,
	View,
} = React;

var styles = StyleSheet.create({
	mainBox: {
		flex: 1
	},
	loading: {
    flex: 1,
    alignSelf: 'center',
  },
	sceneBox: {
		backgroundColor: "#000000",
		height: Display.height - ViewMixin.Dimensions.STATUS_BAR_HEIGHT,
		flex: 1,
		flexDirection: "column",
		padding: 4,
		top: ViewMixin.Dimensions.STATUS_BAR_HEIGHT,
		width: Display.width
	},
	navBar: {
		backgroundColor: "#DF7401"
	}, navBtn: {
		color: "#FFFFFF",
		marginHorizontal: 10,
		fontSize: 32,
		textAlign: "center"
	},
	optionBtn: {
		borderColor: "#A4A4A4",
		borderRadius: 4,
		borderWidth: 1,
		flex: 1,
		flexDirection: "row",
		justifyContent: "center",
		margin: 4,
		padding: 10
	}, optionIcon: {
		color: "#FFFFFF",
		flex: 1,
		fontSize: 32,
		justifyContent: "center"
	}, stBox: {
		flex: 4
	}, optionText: {
		color: "#FFFFFF",
		fontSize: 28,
		justifyContent: "center",
		textAlign: "justify"
	},
	text: {
		color: "#FFFFFF",
		flex: 1,
		textAlign: "center",
	}
});

var BaseConfig = Navigator.SceneConfigs.FloatFromRight
var CustomLeftToRightGesture = _.assign({}, BaseConfig.gestures.pop, {
  snapVelocity: 6,
  edgeHitWidth: Display.width
});

var CustomSceneConfig = _.assign({}, BaseConfig, {
  springTension: 50,
  springFriction: 5,
  gestures: {
    pop: CustomLeftToRightGesture
  }
});

var RLMain = React.createClass({
	mixins: [Reflux.ListenerMixin , Reflux.connect(RequestStore)
				, Reflux.connect(UserStore), RequestMixin, SiteMixin, ViewMixin],
	propTypes: {
		currentUser: PropTypes.object,
		currentSiteRight: PropTypes.object,
		db: PropTypes.object,
		lookups: PropTypes.object,
		sites: PropTypes.object,
		themeColors: PropTypes.array
	},
	prevRequest: null,
	
	getInitialState: function() {
		return {
			nowScene: "list",
			ds: new ListView.DataSource({rowHasChanged: (r1, r2) => r1.guid !== r2.guid}),
			dims: null,
			entityType: "site",
			gettingData: true,
			nav: null,
			prevRequest: null,
			scenes: {
				map: RequestMapScene,
				list: RequestListScene
			},
			showFilter: false,
			searchBar: false
		}
	},

	componentWillMount: function() {
		new Promise.all([this._reloadRequests(), this._reloadAllySites()]).then((results) => {
			console.log("Got all Indirect ally sites");
			this.setState({
				gettingData: false
			});
		})
	},

	componentWillUnmount: function() {
		console.log("Summary Context unMounted");
	},

	shouldComponentUpdate: function(newProps, newState) {
		var updateStatus = false;
		var allyOrgTypeId = this._getAllyOrgTypeId(this.props.currentSiteRight.orgTypeId);

		if (newState.gettingData || !newState.users[this.state.entityType][allyOrgTypeId])
			updateStatus = false;
		
		else if ( !_.eq(newState, this.state) || !_.eq(newProps, this.props));
			updateStatus = true;

		return updateStatus;
	},

	_changeScene: function(scene) {
		if (scene === "map")
			AlertIOS.alert("Stay Tuned!", "Map coming soon...", [
		    {text: 'Okay'}
		  ]);

		this.setState({nowScene: scene});
	},

	// _configureScene: function() {
	// 	return CustomSceneConfig;
	// },

	_openRequest: function(request) {
		// if ( (this._prevRequest != null) && (this._prevRequest.iid === request.iid) )
		// 	nav.jumpForward();
		// else {
	  	var route = {
			  component: RequestDetails,
			  passProps: {
			  	context: "one",
			  	dims: this.state.dims,
			  	omitProps: ["requests"],
			  	request: request,
			  	themeColors: this.props.themeColors
			  }
			};

			this.state.nav.push(route);
		// 	this._prevRequest = request;
		// }
  },

  _reloadAllySites: function() {
  	// get all ally sites of client
		let clientSites = this.props.sites[this._orgTypeIds.CLIENT]
			, orgTypeAllyRefs = _.chain(clientSites).pluck("allies").value().reduce((total, allyRefs) => {
			return _.merge(total, allyRefs);
		});

		let qIndirectOrgTypes = _.mapValues(orgTypeAllyRefs, (siteRefs, orgTypeId) => {
			let siteIds = _.chain(siteRefs).where({"isActive": true}).pluck("id").value();
			return SiteActions.pullAllySites.triggerPromise(orgTypeId, siteIds);
		});
		
		return new Promise.all(qIndirectOrgTypes);
	},

	_reloadRequests: function() {
		var siteRight = this.props.currentSiteRight;
		var orgTypes = this.props.lookups.orgTypes;
		var employerSite = this.props.sites[siteRight.orgTypeId][siteRight.siteId];
		
		return new Promise((resolve, reject) => {
			RequestActions.pullRequests.triggerPromise(employerSite.requests, this.state.entityType)
				.then((requests) => {
	        resolve(requests);
				}).catch((err) => {
					reject("Error getting requests");
				});
		});
	},
	
	_setDims: function(e) {
		if (this.state.dims == null) {
			var layout = e.nativeEvent.layout; 
			
			this.setState({
				dims: {
					height: layout.height,
					width: layout.width,
				}
			});
		} else
			return;
  },

 	_setFilter: function(filterState) {
		var filterStates = this.props.currentUser.settings.filters.statuses.states;
		_.each(filterStates, (state, key) => {
			filterStates[key] = (key === filterState) ? true : false;
		});

		this.setState({showFilter: !this.state.showFilter});
		ProfileActions.setFilter(filterStates);
		RequestActions.refreshRequests("site");
	},

	_toggleSearchBar: function(state) {
		this.setState({searchBar: state});
	},

	_renderFilter: function(option, sectionId, rowId, onIcon, offIcon) {
		var currentUser = this.props.currentUser;
		var filterChoice = _.findKey(currentUser.settings.filters.statuses.states, (value) => {
			return value;
		});

		var Content = (typeof option === "object") ?
			<View style={styles.optionBtn}>
  			{filterChoice === option.iid ? onIcon : offIcon}
      	<View style={styles.stBox}>
      		<Text style={styles.optionText}>{option.name}</Text>
      	</View>
  		</View>
  	: <View style={styles.optionBtn}>
  			{filterChoice === option ? onIcon : offIcon}
      	<View style={styles.stBox}>
      		<Text style={styles.optionText}>{option}</Text>
      	</View>
  		</View>

		return (
			<TouchableHighlight
				key={option.iid}
  			onPress={() => this._setFilter(option.iid || option)}>
  			{Content}
  		</TouchableHighlight>	
		);
	},

	_renderScene: function(route, nav) {
		var navBar, options;

		if ( !_.eq(this.state.nav, nav) )
			this.state.nav = nav;

		if (route.navigationBar) {
		 	navBar = React.addons.cloneWithProps(route.navigationBar, {
		  	navigator: nav,
		  	route: route
		 	});
		}
		
		if (this.state.showFilter) {
			options = _.map(this.Filters, (filter) => {
				return {
					iid: filter,
					name: _.startCase(filter)
				};
			});
		}
		
		var imgHost = this.props.lookups.hosts["images"];
		var Scene = route.component;

		// {...this.props} includes currentUser, currentSiteRight, lookups, and themeColors
		return (
			<View style={styles.mainBox}>
		   	{navBar}
		   	<Modal
	     		animated={false}
	     		transparent={true}
	     		visible={this.state.showFilter}>
	        <MenuSelect
	        	ds={this.state.ds}
	        	options={options}
	        	renderRow={this._renderFilter}
	        	style={styles.sceneBox} />
	      </Modal>
		   	<View style={styles.mainBox} onLayout={this._setDims}>
			   	<Scene
			   		{...this.props}
	   				context="all"
	   				ds={new ListView.DataSource({rowHasChanged: (r1, r2) => r1.guid !== r2.guid})}
	   				imgHost={imgHost}
	   				navigator={nav}
	   				openRequest={this._openRequest}
	   				reloadRequests={this._reloadRequests}
	   				requests={this.state.requests}
	   				route={route}
	   				showSearchBar={this.state.searchBar}
	   				sites={this.props.sites}
	   				users={this.state.users[this.state.entityType]} />
			  </View>
			</View>
		);
	},

	render: function() {		
		if (this.state.gettingData)
			return (
				<ActivityIndicatorIOS
					animating={this.state.isLoading}
					style={styles.loading}
					size="large" />
			);
		else {
			var requestCount = _.toArray(this.state.requests).length;
			var mapBtn =
				<NavBtn onPress={() => this._changeScene("map")}>
					<Icon name={"map"} style={styles.navBtn} />
				</NavBtn>;

			var filterBtn =
				<NavBtn onPress={() => this.setState({showFilter: !this.state.showFilter})}>
					<Icon name={"ios-toggle"} style={styles.navBtn} />
				</NavBtn>;

			var title =
				<NavBtn
					onPress={() => this._toggleSearchBar(!this.state.searchBar) }
					style={{borderColor: "red", borderWidth: 0.75, borderRadius: 2, backgroundColor: "#FFFFFF"}}>
					<Text>Search</Text>
				</NavBtn>;
		
			var navBar =
				<NavBar
					backgroundColor={this.props.themeColors[this.props.currentSiteRight.orgTypeId]}
					buttonsColor="#FFFFFF"
					customPrev={mapBtn}
					customNext={filterBtn}
					customTitle={title}
					titleColor="#FFFFFF" />

			return (
				<Navigator
					configureScene={this._configureScene}
					renderScene={this._renderScene}
					initialRoute={{
					  navigationBar: navBar,
					  component: this.state.scenes[this.state.nowScene]
					}} />
			);
		}
	}
});

module.exports = RLMain;