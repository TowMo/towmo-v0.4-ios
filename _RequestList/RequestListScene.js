'use strict'

// React Native Parts
var React = require('react-native');
var Display = require('react-native-device-display');
var Icon = require("react-native-vector-icons/Ionicons");
var Reflux = require("reflux");
var Refresh = require("react-native-refreshable-listview");
var SearchBar = require("react-native-search-bar");

// CUSTOM COMPONENTS
var Site = require("../Comps/Site");
var Eta = require("../Comps/Eta");
var LineSeparator = require("../Comps/LineSeparator");
var RequestImages = require("../Comps/RequestImages");
var StatusEntry = require("../Comps/StatusEntry");
var User = require("../Comps/User");

// MIXINS
var SiteMixin = require("../Mixins/Site");
var ViewMixin = require("../Mixins/View");

// STORES && ACTIONS

// Utilities
var _ = require("lodash");

var {
	Image,
 	Navigator,
 	PropTypes,
	StyleSheet,
	TouchableHighlight,
	Text,
	View,
} = React;

var _styles = StyleSheet.create({
	request: {
		flexDirection: "column",
		marginHorizontal: 2
	},
	requestBox: {
		borderLeftWidth: 0.5,
		borderRightWidth: 0.5,
		borderTopWidth: 0.5,
		flexDirection: "row",
	}, img: {
		justifyContent: "center",
		flex: 1,
		height: 77,
		resizeMode: "contain",
		width: 77
	}, info: {
		flex: 3,
		flexDirection: "column",
		paddingVertical: 4,
	}, eta: {
		flex: 1,
		flexDirection: "row",
		justifyContent: "center",
		paddingVertical: 4
	},
	todoSection: {
		borderWidth: 0.5,
		flexDirection: "row"
	}, todoIcon: {
			flex: 1,
			fontSize: 26,
			justifyContent: "center",
			marginVertical: 4
		}
});

var statusEntryStyle = StyleSheet.create({
	mainBox: {
		backgroundColor: ViewMixin.Colors.night["block"],
		flexDirection: "row",
		flex: 1,
		padding: 4,
	}, status: {
			flex: 1,
			fontSize: 15
		},
		timeAgo: {
			color: "#A4A4A4",
			flex: 1,
			fontSize: 15,
			textAlign: "right"
		}
});

var RequestListScene = React.createClass({
	propTypes: {
		context: PropTypes.string,
		currentSiteRight: PropTypes.object,
		currentUser: PropTypes.object,
		ds: PropTypes.object,
		imgHost: PropTypes.object,
		lookups: PropTypes.object,
		openRequest: PropTypes.func,
		reloadRequests: PropTypes.func,
		requests: PropTypes.object,
		route: PropTypes.object,
		showSearchBar: PropTypes.bool,
		sites: PropTypes.object,
		themeColors: PropTypes.array,
		users: PropTypes.array
	},
	mixins: [SiteMixin, ViewMixin],
	_imgDims: {
		height: 0,
		width: 60
	},
	getInitialState: function() {
		return {
			requestDims: null,
		}
	},

	componentWillMount: function() {
		this._imgDims.height = this._imgDims.width / this.AspectRatios["1x1"];
	},

	componentWillUnmount: function() {
		console.log("Summary Scene unmounted");
	},

	shouldComponentUpdate: function() {
		return this.props.navigator.getCurrentRoutes().length === 1;
	},

	_renderRequest: function(request, sectionId, rowId) {
		var props = this.props;
		var img = _.findWhere(request.images, {imgTypeId: "vehicle"})
			, clientId = request.sites[this._orgTypeIds.CLIENT].siteId
			, client = props.sites[this._orgTypeIds.CLIENT][clientId]
			, siteRight = props.currentSiteRight
			, imgUri = props.imgHost.url +img.uri +"?fit=crop&w=" +this._imgDims.width +"&h=" +this._imgDims.height
			, lastStatusEntry = _.last(request.statusEntries)
			, site = props.sites[siteRight.orgTypeId][siteRight.siteId]
			, status = props.lookups.statuses[lastStatusEntry.statusId]
			, themeColor = props.themeColors[siteRight.orgTypeId]
			, todos = props.lookups.todos.items
			, user = props.users[lastStatusEntry.author.orgTypeId][lastStatusEntry.author.id]
			, isDoneStyle = !_.has(status, "nextStatuses") ? this.Styles._viewStyle.on : this.Styles._viewStyle.off;
		
		return (
			<TouchableHighlight
				key={rowId}
				underlayColor="#A4A4A4"
				onPress={() => props.openRequest(request)}>
				<View key={rowId} style={_styles.request}>
					<StatusEntry
						currentUser={props.currentUser}
	        	currentSiteRight={siteRight}
						lookups={props.lookups}
						request={request}
						show={{timeAgo: true, status: true}}
						sites={props.sites}
						statusEntry={lastStatusEntry}
						styles={statusEntryStyle}
						themeColors={props.themeColors}
						users={props.users} />	
					<View
						accessibilityOnTap={true}
						key={rowId}
						removeClippedSubviews={true}
						style={ [_styles.requestBox, isDoneStyle] }>
						<Image
							source={{uri: imgUri}}
							style={_styles.img} />
						<View style={_styles.info}>
							<Site
								info={client}
								imgHost={props.imgHost}
								showAddy={{street: true, city: true}}
								showImg={false} />
						</View>
					</View>
					<View style={ [_styles.todoSection, isDoneStyle]}>{
						_.map(request.todoMap, (todoItem, key) => {
							var todoState;
							
							if (todoItem.needed)
								todoState = todoItem.done ? "on" : "need";
							else
								todoState = "n/a";

							return (
								<Icon
									key={todoItem.todoId}
									name={todos[todoItem.todoId].icon}
									style={ [_styles.todoIcon, this.Styles._textStyle[todoState]] } />
							)
						})
					}
					</View>
				</View>
			</TouchableHighlight>
		);
	},

	_renderSeparator: function(section, requestId) {
		// var requests = _.toArray(this.props.requests);
		return (<LineSeparator key={requestId} color="orange" height={0} vertMargin={6} />)
	},

	render: function() {
		var dimensions = this.Dimensions;
		var siteRight = this.props.currentSiteRight;
		var listHeight = Display.height - dimensions.STATUS_BAR_HEIGHT - dimensions.TAB_BAR_HEIGHT - dimensions.NAV_BAR_HEIGHT;
		var themeColor = this.props.themeColors[siteRight.orgTypeId];
		var Header = this.props.showSearchBar ?
			<SearchBar
		    placeholder='Search'
		    onChangeText={(value) => console.log(value)}
		    onSearchButtonPress={() => console.log("run search")}
		    onCancelButtonPress={() => console.log("Close search bar")} /> :
			<User
				employerSite={this.props.sites[siteRight.orgTypeId][siteRight.siteId]}
				imgHost={this.props.imgHost}
				info={this.props.currentUser}
				themeColor={themeColor} />

		return (
			<View style={{height: listHeight}}>
				{Header}
				<Refresh
					contentInset={{top: -this.Dimensions.STATUS_BAR_HEIGHT / 2}}
	        dataSource={this.props.ds.cloneWithRows(this.props.requests)}
	        minDisplayTime={500}
	        minPulldownDistance={30}
	        removeClippedSubviews={true}
	        renderRow={this._renderRequest}
					renderSeparator={this._renderSeparator}
					loadData={this.props.reloadRequests} />
			</View>
		);
	},
});

module.exports = RequestListScene;