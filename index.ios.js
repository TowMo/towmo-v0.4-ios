'use strict';

var Defer = require('deferred');
var Display = require('react-native-device-display');
var Icon = require("react-native-vector-icons/Ionicons");
var Orientation = require("react-native-orientation");
var React = require('react-native');
var Reflux = require("reflux");

// CONTEXTS
var Auth = require("./_Auth/AuthMain");
var NewRequest = require("./_NewRequest/NRMain");
var RequestList = require("./_RequestList/RLMain");
var Settings = require("./_Settings/SMain");
var UserProfile = require("./_UserProfile/UPMain");

// MIXINS
var RequestMixin = require("./Mixins/Request");
var SiteMixin = require("./Mixins/Site");
var ViewMixin = require("./Mixins/View");

// ACTIONS && STORES
var HostStore = require("./Stores/HostStore");
var HostActions = require("./Actions/HostActions");

var LookupActions = require("./Actions/LookupActions");
var LookupStore = require("./Stores/LookupStore");

var LocationStore = require("./Stores/LocationStore");
var LocationActions = require("./Actions/LocationActions");

var ProfileStore = require("./Stores/ProfileStore");
var ProfileActions = require("./Actions/ProfileActions");

var SiteStore = require("./Stores/SiteStore");
var SiteActions = require("./Actions/SiteActions");

var UserActions = require("./Actions/UserActions");
var UserStore = require("./Stores/UserStore");

// Utilities
var Async = require("async");
var Moment = require("moment");
var _ = require("lodash");

var {
  ActivityIndicatorIOS,
  AppRegistry,
  Navigator,
  StatusBarIOS,
  StyleSheet,
  TabBarIOS,
  View,
} = React;

var styles = StyleSheet.create({
  main: {
    backgroundColor: "#000000",
    flex: 1
  },
  navBar: {
    backgroundColor: "#DF7401"
  },
  loading: {
    flex: 1,
    alignSelf: 'center',
  },
  tabBar: {
    borderTopWidth: 1,
    borderColor: "#2E2E2E"
  },
  text: {
    color: "#FFFFFF"
  }
});

var TowMo = React.createClass({
  chosenTab: "requests",
  mixins: [Reflux.connect(HostStore), Reflux.connect(LocationStore)
        , Reflux.connect(LookupStore), Reflux.connect(ProfileStore)
        , Reflux.connect(SiteStore), Reflux.connect(UserStore)
        , Reflux.ListenerMixin, RequestMixin, SiteMixin, ViewMixin],
  getInitialState: function() {
    /***************************************************************
     3 conditions:
      1) Current User populated?
      2) progress of resolving dependencies
      3) dependencies resolved?
    *****************************************************************/
    return {
      inProgress: true,
      badges: {
        "main": {
          newCnt: 0,
        },
        "add": {
          newCnt: 0,
        },
      },
      userInitialized: false
    }
  },

  componentWillMount: function() {
    StatusBarIOS.setStyle("light-content");

    // validate whether user is authenticated w/ Firebase
    // var authData = this.state.db.getAuth();
    Moment.locale('en', {
      relativeTime : {
        future: "in %s",
        past:  "%s ago",
        s:  "< a min",
        m:  "1 min",
        mm: "%d min",
        h:  "1 hr",
        hh: "%d hrs",
        d:  "1 day",
        dd: "%d days",
        M:  "1 month",
        MM: "%d months",
        y:  "1 yr",
        yy: "%d yrs"
      }
    });
    Orientation.lockToPortrait();

    // ProfileActions.logoutUser.triggerPromise();

    // 1. Get Current User Data
    ProfileActions.getAuth.triggerPromise().then((authData) => {      
      ProfileActions.setCurrentUser.triggerPromise(authData).then(() => {
        /***********  NEED TO VERIFY isActive == true *************/
        var status = this._initSession();
        
        if (status === false)
          throw new err("You don't have permission");
        else
          return;
      });
    }).catch(() => {
      this.setState({
        inProgress: false,
      });
      
      console.log("$@@$$@$@$@$@ No Auth Data...");
    });
  },

  componentWillUpdate: function(newProps, newState) {
    var oldState = this.state;

    if (oldState.currentUser && !newState.currentUser) {
      //reset everything
      this.chosenTab = "requests";
      this.setState({
        inProgress: false,
        userInitialized: false,
      });
    }
  },

  _initSession: function() {
    // 1a. Setup GeoWatcher
    LocationActions.setPositionStream();
    var currentUser = this.state.currentUser
      , siteRight = this.state.currentSiteRight;

    let qSites = SiteActions.pullEmployerSite.triggerPromise(siteRight.siteId, siteRight.orgTypeId).then((employerSite) => {
      // 2a1. Get all employer users
      let employerUserIds = _.pluck(employerSite.users, "id");
      let qUsers = UserActions.pullUsers.triggerPromise(siteRight.orgTypeId, "site", employerUserIds);

      // 2a2.  Get all allySites
      let allAllies = _.omit(employerSite.allies, siteRight.orgTypeId);
      let qAllyOrgTypes = _.map(allAllies, (allies, orgType) => {
        var siteIds = _.keys(allies);

        // pull direct ally sites
        return SiteActions.pullAllySites.triggerPromise(orgType, siteIds).then((allySites) => {
          // get all ally users
          let usersOfAllySites = _.pluck(allySites, "users");
          
          if (_.isEmpty(usersOfAllySites))
            return new Promise.reject("Weird:  No ", orgType +" sites have users");
          else {
            var userLists = _.map(usersOfAllySites, (users) => {
              return _.toArray(users);
            });

            var allyUserIds = _.chain(userLists).flatten(userLists).pluck("id").value();
            return UserActions.pullUsers(orgType, "site", allyUserIds);
          }
        });
      });

      return new Promise.all([qUsers, new Promise.all(qAllyOrgTypes)]);
    });
    
    // 3a. populate employer sites
    let qLookups = LookupActions.validateLookups.triggerPromise().then((lookups) => {
      
      // 3a1. Get S3 Policy for uploading images
      return HostActions.pullS3Policy(lookups.hosts["s3Policy"]);
    })
    // .catch((err) => {
    //   var errMsg = "Could not validate Lookups: " +err;
    //   console.log(errMsg);
    //   return;
    // });

    new Promise.all([qSites, qLookups]).then((results) => {
      this.setState({
        inProgress: false,
        userInitialized: true,
      });
    }).catch((err) => {
      console.log("Overall Error:  couldn't get a lot of shit: ", err);
    });

      //   console.log("Employer Site and Lookups were obatined!");
      // }
    // });
  },

  _updateBadgeCount: function(tab, cnt) {
    this.setState((prevState, currentProps) => {
      var badges = _.transform(prevState.badges, (result, badge, key) => {
        result[tab] = {"newCnt": (tab == key) ? cnt : prevState.badges[key].newCnt};
      });
      
      return badges;
    });
  },

  _routeContext: function(dest) {
    var Context = null;
    var orgTypes = this.state.lookups.orgTypes;
    var themeColors = new Array(_.toArray(orgTypes).length);

    _.each(orgTypes, (orgType) => {
      themeColors[orgType.iid] = orgType.color;
    });
    
    switch(this.chosenTab) {
      case "requests":
        Context = RequestList;
        break;
      
      case "new":
        Context = NewRequest;
        break;

      case "profile":
        Context = UserProfile;
        break;

      case "settings":
        Context = Settings;
        break;

      default:
        Context = RequestList;
        break;
    }

    return (
      <Context
        currentSiteRight={this.state.currentSiteRight}
        currentUser={this.state.currentUser}
        db={this.state.db}
        lookups={this.state.lookups}
        sites={this.state.sites}
        themeColors={themeColors} />
    );
  },

  _setInProgress: function(state) {
    this.setState({
      inProgress: state,
    })
  },

  _setTab: function(tab) {
    this.chosenTab = tab;
    this.setState({
      notifCount: this.state.notifCount + 1,
    });
  },

  render: function() {
    if (!this.state.inProgress && !this.state.currentUser)
      return (
        <View style={styles.main}>
          <Auth
            initSession={this._initSession}
            setInProgress={this._setInProgress} />
        </View>
      );
    else if (this.state.inProgress)
      return (
        <ActivityIndicatorIOS
          animating={true}
          style={styles.loading}
          size="large" />
      );
    else {
      var lookups = this.state.lookups;
      var siteRight = this.state.currentSiteRight;
      var themeColor = lookups.orgTypes[siteRight.orgTypeId].color;
      
      var requestsTabBarItem = _.contains(siteRight.tasks, "task5") && lookups.tasks["task5"].orgTypeIds[siteRight.orgTypeId] ?
        <Icon.TabBarItem
          iconName="model-s"
          onPress={() => this._setTab("requests")}
          selected={this.chosenTab === 'requests'}
          title="Requests">{this.chosenTab == "requests" ? this._routeContext("requests") : ""}
        </Icon.TabBarItem> : null;
      
      return (
        <TabBarIOS
          barTintColor="#1C1C1C"
          style={styles.main}
          tintColor={themeColor}
          translucent={true}>    
          
          {requestsTabBarItem}

          <Icon.TabBarItem
            badge={this.state.notifCount > 0 ? this.state.notifCount : undefined}
            iconName="ios-compose"
            onPress={() => this._setTab("new")}
            selected={this.chosenTab === "new"}
            title="New">
              {this.chosenTab === "new" ? this._routeContext("new") : ""}
          </Icon.TabBarItem>

          <Icon.TabBarItem
            iconName="ios-person"
            onPress={() => this._setTab("profile")}
            selected={this.chosenTab === 'profile'}
            title="Profile">
              {this.chosenTab === "profile" ? this._routeContext("profile") : ""}
          </Icon.TabBarItem>

          <Icon.TabBarItem
            iconName="ios-settings"
            onPress={() => this._setTab("settings")}
            selected={this.chosenTab === 'settings'}
            title="Settings">
              {this.chosenTab === "settings" ? this._routeContext("settings") : ""}
          </Icon.TabBarItem>
        </TabBarIOS>
      );
    }
  }
});

AppRegistry.registerComponent('TowMo', () => TowMo);
